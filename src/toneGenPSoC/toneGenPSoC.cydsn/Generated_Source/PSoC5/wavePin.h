/*******************************************************************************
* File Name: wavePin.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_wavePin_H) /* Pins wavePin_H */
#define CY_PINS_wavePin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "wavePin_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 wavePin__PORT == 15 && ((wavePin__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    wavePin_Write(uint8 value);
void    wavePin_SetDriveMode(uint8 mode);
uint8   wavePin_ReadDataReg(void);
uint8   wavePin_Read(void);
void    wavePin_SetInterruptMode(uint16 position, uint16 mode);
uint8   wavePin_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the wavePin_SetDriveMode() function.
     *  @{
     */
        #define wavePin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define wavePin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define wavePin_DM_RES_UP          PIN_DM_RES_UP
        #define wavePin_DM_RES_DWN         PIN_DM_RES_DWN
        #define wavePin_DM_OD_LO           PIN_DM_OD_LO
        #define wavePin_DM_OD_HI           PIN_DM_OD_HI
        #define wavePin_DM_STRONG          PIN_DM_STRONG
        #define wavePin_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define wavePin_MASK               wavePin__MASK
#define wavePin_SHIFT              wavePin__SHIFT
#define wavePin_WIDTH              1u

/* Interrupt constants */
#if defined(wavePin__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in wavePin_SetInterruptMode() function.
     *  @{
     */
        #define wavePin_INTR_NONE      (uint16)(0x0000u)
        #define wavePin_INTR_RISING    (uint16)(0x0001u)
        #define wavePin_INTR_FALLING   (uint16)(0x0002u)
        #define wavePin_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define wavePin_INTR_MASK      (0x01u) 
#endif /* (wavePin__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define wavePin_PS                     (* (reg8 *) wavePin__PS)
/* Data Register */
#define wavePin_DR                     (* (reg8 *) wavePin__DR)
/* Port Number */
#define wavePin_PRT_NUM                (* (reg8 *) wavePin__PRT) 
/* Connect to Analog Globals */                                                  
#define wavePin_AG                     (* (reg8 *) wavePin__AG)                       
/* Analog MUX bux enable */
#define wavePin_AMUX                   (* (reg8 *) wavePin__AMUX) 
/* Bidirectional Enable */                                                        
#define wavePin_BIE                    (* (reg8 *) wavePin__BIE)
/* Bit-mask for Aliased Register Access */
#define wavePin_BIT_MASK               (* (reg8 *) wavePin__BIT_MASK)
/* Bypass Enable */
#define wavePin_BYP                    (* (reg8 *) wavePin__BYP)
/* Port wide control signals */                                                   
#define wavePin_CTL                    (* (reg8 *) wavePin__CTL)
/* Drive Modes */
#define wavePin_DM0                    (* (reg8 *) wavePin__DM0) 
#define wavePin_DM1                    (* (reg8 *) wavePin__DM1)
#define wavePin_DM2                    (* (reg8 *) wavePin__DM2) 
/* Input Buffer Disable Override */
#define wavePin_INP_DIS                (* (reg8 *) wavePin__INP_DIS)
/* LCD Common or Segment Drive */
#define wavePin_LCD_COM_SEG            (* (reg8 *) wavePin__LCD_COM_SEG)
/* Enable Segment LCD */
#define wavePin_LCD_EN                 (* (reg8 *) wavePin__LCD_EN)
/* Slew Rate Control */
#define wavePin_SLW                    (* (reg8 *) wavePin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define wavePin_PRTDSI__CAPS_SEL       (* (reg8 *) wavePin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define wavePin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) wavePin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define wavePin_PRTDSI__OE_SEL0        (* (reg8 *) wavePin__PRTDSI__OE_SEL0) 
#define wavePin_PRTDSI__OE_SEL1        (* (reg8 *) wavePin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define wavePin_PRTDSI__OUT_SEL0       (* (reg8 *) wavePin__PRTDSI__OUT_SEL0) 
#define wavePin_PRTDSI__OUT_SEL1       (* (reg8 *) wavePin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define wavePin_PRTDSI__SYNC_OUT       (* (reg8 *) wavePin__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(wavePin__SIO_CFG)
    #define wavePin_SIO_HYST_EN        (* (reg8 *) wavePin__SIO_HYST_EN)
    #define wavePin_SIO_REG_HIFREQ     (* (reg8 *) wavePin__SIO_REG_HIFREQ)
    #define wavePin_SIO_CFG            (* (reg8 *) wavePin__SIO_CFG)
    #define wavePin_SIO_DIFF           (* (reg8 *) wavePin__SIO_DIFF)
#endif /* (wavePin__SIO_CFG) */

/* Interrupt Registers */
#if defined(wavePin__INTSTAT)
    #define wavePin_INTSTAT            (* (reg8 *) wavePin__INTSTAT)
    #define wavePin_SNAP               (* (reg8 *) wavePin__SNAP)
    
	#define wavePin_0_INTTYPE_REG 		(* (reg8 *) wavePin__0__INTTYPE)
#endif /* (wavePin__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_wavePin_H */


/* [] END OF FILE */
