/*******************************************************************************
* File Name: toneGen_ctrlReg_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "toneGen_ctrlReg.h"

/* Check for removal by optimization */
#if !defined(toneGen_ctrlReg_Sync_ctrl_reg__REMOVED)

static toneGen_ctrlReg_BACKUP_STRUCT  toneGen_ctrlReg_backup = {0u};

    
/*******************************************************************************
* Function Name: toneGen_ctrlReg_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_ctrlReg_SaveConfig(void) 
{
    toneGen_ctrlReg_backup.controlState = toneGen_ctrlReg_Control;
}


/*******************************************************************************
* Function Name: toneGen_ctrlReg_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void toneGen_ctrlReg_RestoreConfig(void) 
{
     toneGen_ctrlReg_Control = toneGen_ctrlReg_backup.controlState;
}


/*******************************************************************************
* Function Name: toneGen_ctrlReg_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_ctrlReg_Sleep(void) 
{
    toneGen_ctrlReg_SaveConfig();
}


/*******************************************************************************
* Function Name: toneGen_ctrlReg_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_ctrlReg_Wakeup(void)  
{
    toneGen_ctrlReg_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
