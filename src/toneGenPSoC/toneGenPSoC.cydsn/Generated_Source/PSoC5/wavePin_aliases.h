/*******************************************************************************
* File Name: wavePin.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_wavePin_ALIASES_H) /* Pins wavePin_ALIASES_H */
#define CY_PINS_wavePin_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define wavePin_0			(wavePin__0__PC)
#define wavePin_0_INTR	((uint16)((uint16)0x0001u << wavePin__0__SHIFT))

#define wavePin_INTR_ALL	 ((uint16)(wavePin_0_INTR))

#endif /* End Pins wavePin_ALIASES_H */


/* [] END OF FILE */
