/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "toneGen.h"

void toneGen_Start() {}
void toneGen_Init() {}

void toneGen_Write_ctrl(uint8 ctrl) {}
void toneGen_Write_stepTriSaw(uint8 stepTriSaw) {}
void toneGen_Write_clkCnt(uint16 clkCnt) {}
void toneGen_Write_dtNum(uint8 dtNum) {}

uint8 toneGen_Read_ctrl() {
    return 16;
}
uint8 toneGen_Read_stepTriSaw() {
    return 1;    
}
uint16 toneGen_Read_clkCnt() {
    return 1;
}
uint16 toneGen_Read_dtNum() {
    return 1;
}

/* [] END OF FILE */
