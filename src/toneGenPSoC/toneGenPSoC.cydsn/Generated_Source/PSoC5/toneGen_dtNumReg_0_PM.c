/*******************************************************************************
* File Name: toneGen_dtNumReg_0_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "toneGen_dtNumReg_0.h"

/* Check for removal by optimization */
#if !defined(toneGen_dtNumReg_0_Sync_ctrl_reg__REMOVED)

static toneGen_dtNumReg_0_BACKUP_STRUCT  toneGen_dtNumReg_0_backup = {0u};

    
/*******************************************************************************
* Function Name: toneGen_dtNumReg_0_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_dtNumReg_0_SaveConfig(void) 
{
    toneGen_dtNumReg_0_backup.controlState = toneGen_dtNumReg_0_Control;
}


/*******************************************************************************
* Function Name: toneGen_dtNumReg_0_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void toneGen_dtNumReg_0_RestoreConfig(void) 
{
     toneGen_dtNumReg_0_Control = toneGen_dtNumReg_0_backup.controlState;
}


/*******************************************************************************
* Function Name: toneGen_dtNumReg_0_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_dtNumReg_0_Sleep(void) 
{
    toneGen_dtNumReg_0_SaveConfig();
}


/*******************************************************************************
* Function Name: toneGen_dtNumReg_0_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_dtNumReg_0_Wakeup(void)  
{
    toneGen_dtNumReg_0_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
