/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"

void toneGen_1_Start();
void toneGen_1_Init();

void toneGen_1_Write_ctrl(uint8 ctrl);
void toneGen_1_Write_stepTriSaw(uint8 stepTriSaw);
void toneGen_1_Write_clkCnt(uint16 clkCnt);
void toneGen_1_Write_dtNum(uint8 dtNum);

uint8 toneGen_1_Read_ctrl();
uint8 toneGen_1_Read_stepTriSaw();
uint16 toneGen_1_Read_clkCnt();
uint16 toneGen_1_Read_dtNum();

/* [] END OF FILE */
