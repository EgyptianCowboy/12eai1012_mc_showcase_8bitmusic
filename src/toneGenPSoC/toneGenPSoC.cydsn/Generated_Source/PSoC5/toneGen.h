/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef toneGen_H
    #define toneGen_H
    #include "toneGen_defs.h"

    /**************************************
    *           Parameter Defaults
    **************************************/

    #define toneGen_ctrl_default           16u
    #define toneGen_stepTriSaw_default     1u
    #define toneGen_clkCnt_default         1u
    #define toneGen_dtNum_default          1u

    /**************************************
    *           Define registers
    **************************************/
    
//    #define toneGen_ctrl                   (* (reg8*) toneGen_ctrlReg__CONTROL_REG)
//    #define toneGen_stepTriSaw             (* (reg8*) toneGen_stepTSReg__CONTROL_REG)
//    #define toneGen_clkCnt_0               (* (reg8*) toneGen_clkCntReg_0__CONTROL_REG)
//    #define toneGen_clkCnt_1               (* (reg8*) toneGen_clkCntReg_1__CONTROL_REG)
//    #define toneGen_dtNum_0                (* (reg8*) toneGen_dtNumReg_0__CONTROL_REG)
//    #define toneGen_dtNum_1                (* (reg8*) toneGen_dtNumReg_1__CONTROL_REG)
//    
//    #define toneGen_ctrl_PTR               ((reg8*) toneGen_ctrlReg__CONTROL_REG)
//    #define toneGen_stepTriSaw_PTR         ((reg8*) toneGen_stepTSReg__CONTROL_REG)
//    #define toneGen_clkCnt_0_PTR           ((reg8*) toneGen_clkCntReg_0__CONTROL_REG)
//    #define toneGen_clkCnt_1_PTR           ((reg8*) toneGen_clkCntReg_1__CONTROL_REG)
//    #define toneGen_dtNum_0_PTR            ((reg8*) toneGen_dtNumReg_0__CONTROL_REG)
//    #define toneGen_dtNum_1_PTR            ((reg8*) toneGen_dtNumReg_1__CONTROL_REG)
    

    //#define toneGen_ctrlReg_Sync_ctrl_reg__CONTROL_REG

    void toneGen_Start();
    void toneGen_Init();

    void toneGen_ctrl_Write(uint8 ctrl);
    void toneGen_stepTriSaw_Write(uint8 stepTriSaw);
    void toneGen_clkCnt_Write(uint16 clkCnt);
    void toneGen_dtNum_Write(uint8 dtNum);

    uint8 toneGen_ctrl_Read();
    uint8 toneGen_stepTriSaw_Read();
    uint16 toneGen_clkCnt_Read();
    uint16 toneGen_dtNum_Read();
#endif
/* [] END OF FILE */
