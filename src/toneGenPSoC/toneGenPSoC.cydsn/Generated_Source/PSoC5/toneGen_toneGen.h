/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"

/**************************************
*           Parameter Defaults
**************************************/

#define toneGen_ctrl           16u
#define toneGen_stepTriSaw     1u
#define toneGen_clkCnt         1u
#define toneGen_dtNum          1u



#define $INSTANCE_NAME`_Start();
void toneGen_Init();

void toneGen_Write_ctrl(uint8 ctrl);
void toneGen_Write_stepTriSaw(uint8 stepTriSaw);
void toneGen_Write_clkCnt(uint16 clkCnt);
void toneGen_Write_dtNum(uint8 dtNum);

uint8 toneGen_Read_ctrl();
uint8 toneGen_Read_stepTriSaw();
uint16 toneGen_Read_clkCnt();
uint16 toneGen_Read_dtNum();

/* [] END OF FILE */
