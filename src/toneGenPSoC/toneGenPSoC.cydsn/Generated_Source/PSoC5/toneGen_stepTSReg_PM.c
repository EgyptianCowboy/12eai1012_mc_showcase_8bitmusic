/*******************************************************************************
* File Name: toneGen_stepTSReg_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "toneGen_stepTSReg.h"

/* Check for removal by optimization */
#if !defined(toneGen_stepTSReg_Sync_ctrl_reg__REMOVED)

static toneGen_stepTSReg_BACKUP_STRUCT  toneGen_stepTSReg_backup = {0u};

    
/*******************************************************************************
* Function Name: toneGen_stepTSReg_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_stepTSReg_SaveConfig(void) 
{
    toneGen_stepTSReg_backup.controlState = toneGen_stepTSReg_Control;
}


/*******************************************************************************
* Function Name: toneGen_stepTSReg_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void toneGen_stepTSReg_RestoreConfig(void) 
{
     toneGen_stepTSReg_Control = toneGen_stepTSReg_backup.controlState;
}


/*******************************************************************************
* Function Name: toneGen_stepTSReg_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_stepTSReg_Sleep(void) 
{
    toneGen_stepTSReg_SaveConfig();
}


/*******************************************************************************
* Function Name: toneGen_stepTSReg_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void toneGen_stepTSReg_Wakeup(void)  
{
    toneGen_stepTSReg_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
