/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "toneGen.h"

void toneGen_Start() {}
void toneGen_Init() {
//    toneGen_ctrl = toneGen_ctrl_default;
//    toneGen_stepTriSaw = toneGen_stepTriSaw_default;
}

void toneGen_ctrl_Write(uint8 ctrl) {}

void toneGen_stepTriSaw_Write(uint8 stepTriSaw) {}
void toneGen_clkCnt_Write(uint16 clkCnt) {}
void toneGen_dtNum_Write(uint8 dtNum) {}

uint8 toneGen_Read_ctrl_Read() {
    //toneGen_ctrl;
}
uint8 toneGen_Read_stepTriSaw_Read() {
    //toneGen_stepTriSaw;    
}
uint16 toneGen_Read_clkCnt_Read() {
    //return 1;
}
uint16 toneGen_Read_dtNum_Read() {
    //return 1;
}

/* [] END OF FILE */
