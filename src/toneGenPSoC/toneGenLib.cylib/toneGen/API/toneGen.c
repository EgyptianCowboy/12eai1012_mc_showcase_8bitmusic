/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "`$INSTANCE_NAME`.h"

void `$INSTANCE_NAME`_Start() {}
void `$INSTANCE_NAME`_Init() {
//    `$INSTANCE_NAME`_ctrl = `$INSTANCE_NAME`_ctrl_default;
//    `$INSTANCE_NAME`_stepTriSaw = `$INSTANCE_NAME`_stepTriSaw_default;
}

void `$INSTANCE_NAME`_ctrl_Write(uint8 ctrl) {}

void `$INSTANCE_NAME`_stepTriSaw_Write(uint8 stepTriSaw) {}
void `$INSTANCE_NAME`_clkCnt_Write(uint16 clkCnt) {}
void `$INSTANCE_NAME`_dtNum_Write(uint8 dtNum) {}

uint8 `$INSTANCE_NAME`_Read_ctrl_Read() {
    //`$INSTANCE_NAME`_ctrl;
}
uint8 `$INSTANCE_NAME`_Read_stepTriSaw_Read() {
    //`$INSTANCE_NAME`_stepTriSaw;    
}
uint16 `$INSTANCE_NAME`_Read_clkCnt_Read() {
    //return `$clkCnt`;
}
uint16 `$INSTANCE_NAME`_Read_dtNum_Read() {
    //return `$dtNum`;
}

/* [] END OF FILE */
