/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef `$INSTANCE_NAME`_H
    #define `$INSTANCE_NAME`_H
    #include "`$INSTANCE_NAME`_defs.h"

    /**************************************
    *           Parameter Defaults
    **************************************/

    #define `$INSTANCE_NAME`_ctrl_default           `$ctrl`u
    #define `$INSTANCE_NAME`_stepTriSaw_default     `$stepTriSaw`u
    #define `$INSTANCE_NAME`_clkCnt_default         `$clkCnt`u
    #define `$INSTANCE_NAME`_dtNum_default          `$dtNum`u

    /**************************************
    *           Define registers
    **************************************/
    
//    #define `$INSTANCE_NAME`_ctrl                   (* (reg8*) `$INSTANCE_NAME`_ctrlReg__CONTROL_REG)
//    #define `$INSTANCE_NAME`_stepTriSaw             (* (reg8*) `$INSTANCE_NAME`_stepTSReg__CONTROL_REG)
//    #define `$INSTANCE_NAME`_clkCnt_0               (* (reg8*) `$INSTANCE_NAME`_clkCntReg_0__CONTROL_REG)
//    #define `$INSTANCE_NAME`_clkCnt_1               (* (reg8*) `$INSTANCE_NAME`_clkCntReg_1__CONTROL_REG)
//    #define `$INSTANCE_NAME`_dtNum_0                (* (reg8*) `$INSTANCE_NAME`_dtNumReg_0__CONTROL_REG)
//    #define `$INSTANCE_NAME`_dtNum_1                (* (reg8*) `$INSTANCE_NAME`_dtNumReg_1__CONTROL_REG)
//    
//    #define `$INSTANCE_NAME`_ctrl_PTR               ((reg8*) `$INSTANCE_NAME`_ctrlReg__CONTROL_REG)
//    #define `$INSTANCE_NAME`_stepTriSaw_PTR         ((reg8*) `$INSTANCE_NAME`_stepTSReg__CONTROL_REG)
//    #define `$INSTANCE_NAME`_clkCnt_0_PTR           ((reg8*) `$INSTANCE_NAME`_clkCntReg_0__CONTROL_REG)
//    #define `$INSTANCE_NAME`_clkCnt_1_PTR           ((reg8*) `$INSTANCE_NAME`_clkCntReg_1__CONTROL_REG)
//    #define `$INSTANCE_NAME`_dtNum_0_PTR            ((reg8*) `$INSTANCE_NAME`_dtNumReg_0__CONTROL_REG)
//    #define `$INSTANCE_NAME`_dtNum_1_PTR            ((reg8*) `$INSTANCE_NAME`_dtNumReg_1__CONTROL_REG)
    

    //#define `$INSTANCE_NAME`_ctrlReg_Sync_ctrl_reg__CONTROL_REG

    void `$INSTANCE_NAME`_Start();
    void `$INSTANCE_NAME`_Init();

    void `$INSTANCE_NAME`_ctrl_Write(uint8 ctrl);
    void `$INSTANCE_NAME`_stepTriSaw_Write(uint8 stepTriSaw);
    void `$INSTANCE_NAME`_clkCnt_Write(uint16 clkCnt);
    void `$INSTANCE_NAME`_dtNum_Write(uint8 dtNum);

    uint8 `$INSTANCE_NAME`_ctrl_Read();
    uint8 `$INSTANCE_NAME`_stepTriSaw_Read();
    uint16 `$INSTANCE_NAME`_clkCnt_Read();
    uint16 `$INSTANCE_NAME`_dtNum_Read();
#endif
/* [] END OF FILE */
