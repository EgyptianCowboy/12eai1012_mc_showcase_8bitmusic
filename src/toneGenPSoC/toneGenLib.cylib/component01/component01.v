//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line
// Generated on 12/01/2018 at 17:49
// Component: toneGen
module toneGen (
	output reg [7:0] outLevel,
	input  [15:0] clkCnt,
	input   clock,
	input  [15:0] dtNum
);
	parameter ctrl = 1;
	parameter stepTriSaw = 1;

//`#start body` -- edit after this line, do not edit this line
   reg [15:0]		counter;
   reg [7:0]		sawTriCnt;
   reg [7:0]        stepTriSawInt; 
   reg [7:0]        ctrlInt; 
   
   wire			    linear_feedback;
   
   assign linear_feedback =  ! (outLevel[7] ^ outLevel[3]);

   cy_psoc3_control #(.cy_init_value (8'b00000000), .cy_force_order(`TRUE))
      ctrlReg(
      /* output [07:00] */ .control(ctrlInt)
   );
   
   cy_psoc3_control #(.cy_init_value (8'b00000000), .cy_force_order(`TRUE))
      stepTSReg(
      /* output [07:00] */ .control(stepTriSawInt)
   );

   always@(posedge clock)
     begin
	if((counter == clkCnt) || ctrlInt[4])
	  begin
	     outLevel <= 8'h00;
	     counter <= 16'h0000;
	     sawTriCnt <= 8'h00;
	  end
	else
	  begin
	     case(ctrlInt & 8'h0F)
	       // Noise
	       8'h01 :
		 begin
		    if(sawTriCnt == stepTriSawInt)
		      begin
			 outLevel <= {outLevel[6],outLevel[5], outLevel[4], outLevel[3], outLevel[2], outLevel[1], outLevel[0], linear_feedback};
			 sawTriCnt <= 8'h00;
		      end
		    else
		      begin
			 sawTriCnt <= sawTriCnt+1;
		      end
		 end // case: 8'h01
	       // Pulse
	       8'h02 :
		 begin
		    if(counter == dtNum)
		      outLevel <= 8'h00;
		    else if(counter == 16'h0000)
		      outLevel <= 8'hFF;
		 end
	       // Sawtooth
	       8'h04 :
		 begin
		    if(clkCnt > 16'h00FF)
		      begin
			 if(sawTriCnt == stepTriSawInt)
			   begin
			      outLevel <= outLevel+1;
			      sawTriCnt <= 8'h00;
			   end
			 else
			   sawTriCnt <= sawTriCnt+1;
		      end
		    else
		      begin
			 outLevel <= outLevel+1;
		      end // else: !if(clkCnt > 8'hFF)
		 end // case: 8'h04
	       // Triangle
	       8'h08 :
		 begin
		    if(clkCnt > 16'h00FF)
		      begin
			 if(sawTriCnt == stepTriSawInt)
			   begin
			      if(counter <= (clkCnt >> 1))
				begin
				   outLevel <= outLevel+1;
				   sawTriCnt <= 8'h00;
				end
			      else if(counter > (clkCnt >>1))
				begin
				   outLevel <= outLevel-1;
				   sawTriCnt <= 8'h00;
				end // else: !if(counter < (clkCnt >> 1))
			   end // if (sawTriCnt == stepTriSawInt)
			 else
			   sawTriCnt <= sawTriCnt+1;
		      end // if (clkCnt > 16'h00FF)
		    else
		      begin
			 outLevel <= outLevel+1;
		      end // else: !if(clkCnt > 16'h00FF)
		 end // case: 8'h08
	       default:
		 outLevel <= 8'h00;
	     endcase
	     counter <= counter + 1;
	  end // else: !if((counter == clkCnt) || (ctrlInt & 8'h10) == 8'h10)
     end // always@ (posedge clk)
//`#end` -- edit above this line, do not edit this line
endmodule
//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line
