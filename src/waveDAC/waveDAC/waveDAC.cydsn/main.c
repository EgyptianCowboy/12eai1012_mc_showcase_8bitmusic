/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE->
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company->
 *
 * ========================================
*/
#include "toneGen.h"

#define TONESAMPLESIZE 255

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts-> */
    /* Place your initialization/startup code here (e->g-> MyInst_Start()) */
    uint8 DT = 50;
    pulseDtCnt pulseCnts;
    clkParam clkParameters;
    clkParameters.clk1Div = clockDiv(10000);
    clkParameters.clk2Div = clockDiv(10000);
    clkParameters.clk3Div = clockDiv(10000);
    pulseCnts.pulse1DtCnt = setDtCnt(DT);
    pulseCnts.pulse2DtCnt = setDtCnt(DT);
    pulseCnts.pulse3DtCnt = setDtCnt(DT);
    
    toneArr localTones;
    //localTones = malloc(sizeof(localTones));
    localTones.arrSize = sizeof(localTones.triArray);
    
    fillSaw((&localTones)->sawArray, (&localTones)->arrSize);
    fillTri((&localTones)->triArray, (&localTones)->arrSize);
    fillNoise((&localTones)->noiseArray, (&localTones)->arrSize);
    fillPulse((&localTones)->pulse1Array, (&localTones)->arrSize, pulseCnts.pulse1DtCnt);
    fillPulse((&localTones)->pulse2Array, (&localTones)->arrSize, pulseCnts.pulse2DtCnt);
    fillPulse((&localTones)->pulse3Array, (&localTones)->arrSize, pulseCnts.pulse3DtCnt);
    clkTone1_SetDivider(clkParameters.clk1Div);
    clkTone2_SetDivider(clkParameters.clk2Div);
    clkTone3_SetDivider(clkParameters.clk3Div);
    
    initToneGen();
    stopTones();
    toneGen1_Wave1Setup(localTones.triArray,localTones.arrSize);
    toneGen2_Wave1Setup(localTones.noiseArray,localTones.arrSize);
    toneGen3_Wave1Setup(localTones.sawArray,localTones.arrSize);
    startTones(5);
    for(;;)
    {   
        
        /* Place your application code here-> */
        
    }
}

/* [] END OF FILE */