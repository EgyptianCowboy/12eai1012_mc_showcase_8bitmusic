/*******************************************************************************
* File Name: toneGen2.h  
* Version 2.10
*
* Description:
*  This file contains the function prototypes and constants used in
*  the 8-bit Waveform DAC (WaveDAC8) Component.
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_WaveDAC8_toneGen2_H) 
#define CY_WaveDAC8_toneGen2_H

#include "cytypes.h"
#include "cyfitter.h"
#include <toneGen2_Wave1_DMA_dma.h>
#include <toneGen2_Wave2_DMA_dma.h>
#include <toneGen2_VDAC8.h>


/***************************************
*  Initial Parameter Constants
***************************************/

#define toneGen2_WAVE1_TYPE     (0u)     /* Waveform for wave1 */
#define toneGen2_WAVE2_TYPE     (2u)     /* Waveform for wave2 */
#define toneGen2_SINE_WAVE      (0u)
#define toneGen2_SQUARE_WAVE    (1u)
#define toneGen2_TRIANGLE_WAVE  (2u)
#define toneGen2_SAWTOOTH_WAVE  (3u)
#define toneGen2_ARB_DRAW_WAVE  (10u) /* Arbitrary (draw) */
#define toneGen2_ARB_FILE_WAVE  (11u) /* Arbitrary (from file) */

#define toneGen2_WAVE1_LENGTH   (100u)   /* Length for wave1 */
#define toneGen2_WAVE2_LENGTH   (100u)   /* Length for wave2 */
	
#define toneGen2_DEFAULT_RANGE    (1u) /* Default DAC range */
#define toneGen2_DAC_RANGE_1V     (0u)
#define toneGen2_DAC_RANGE_1V_BUF (16u)
#define toneGen2_DAC_RANGE_4V     (1u)
#define toneGen2_DAC_RANGE_4V_BUF (17u)
#define toneGen2_VOLT_MODE        (0u)
#define toneGen2_CURRENT_MODE     (1u)
#define toneGen2_DAC_MODE         (((toneGen2_DEFAULT_RANGE == toneGen2_DAC_RANGE_1V) || \
									  (toneGen2_DEFAULT_RANGE == toneGen2_DAC_RANGE_4V) || \
							  		  (toneGen2_DEFAULT_RANGE == toneGen2_DAC_RANGE_1V_BUF) || \
									  (toneGen2_DEFAULT_RANGE == toneGen2_DAC_RANGE_4V_BUF)) ? \
									   toneGen2_VOLT_MODE : toneGen2_CURRENT_MODE)

#define toneGen2_DACMODE toneGen2_DAC_MODE /* legacy definition for backward compatibility */

#define toneGen2_DIRECT_MODE (0u)
#define toneGen2_BUFFER_MODE (1u)
#define toneGen2_OUT_MODE    (((toneGen2_DEFAULT_RANGE == toneGen2_DAC_RANGE_1V_BUF) || \
								 (toneGen2_DEFAULT_RANGE == toneGen2_DAC_RANGE_4V_BUF)) ? \
								  toneGen2_BUFFER_MODE : toneGen2_DIRECT_MODE)

#if(toneGen2_OUT_MODE == toneGen2_BUFFER_MODE)
    #include <toneGen2_BuffAmp.h>
#endif /* toneGen2_OUT_MODE == toneGen2_BUFFER_MODE */

#define toneGen2_CLOCK_INT      (1u)
#define toneGen2_CLOCK_EXT      (0u)
#define toneGen2_CLOCK_SRC      (0u)

#if(toneGen2_CLOCK_SRC == toneGen2_CLOCK_INT)  
	#include <toneGen2_DacClk.h>
	#if defined(toneGen2_DacClk_PHASE)
		#define toneGen2_CLK_PHASE_0nS (1u)
	#endif /* defined(toneGen2_DacClk_PHASE) */
#endif /* toneGen2_CLOCK_SRC == toneGen2_CLOCK_INT */

#if (CY_PSOC3)
	#define toneGen2_HI16FLASHPTR   (0xFFu)
#endif /* CY_PSOC3 */

#define toneGen2_Wave1_DMA_BYTES_PER_BURST      (1u)
#define toneGen2_Wave1_DMA_REQUEST_PER_BURST    (1u)
#define toneGen2_Wave2_DMA_BYTES_PER_BURST      (1u)
#define toneGen2_Wave2_DMA_REQUEST_PER_BURST    (1u)


/***************************************
*   Data Struct Definition
***************************************/

/* Low power Mode API Support */
typedef struct
{
	uint8   enableState;
}toneGen2_BACKUP_STRUCT;


/***************************************
*        Function Prototypes 
***************************************/

void toneGen2_Start(void)             ;
void toneGen2_StartEx(const uint8 * wavePtr1, uint16 sampleSize1, const uint8 * wavePtr2, uint16 sampleSize2)
                                        ;
void toneGen2_Init(void)              ;
void toneGen2_Enable(void)            ;
void toneGen2_Stop(void)              ;

void toneGen2_Wave1Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;
void toneGen2_Wave2Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;

void toneGen2_Sleep(void)             ;
void toneGen2_Wakeup(void)            ;

#define toneGen2_SetSpeed       toneGen2_VDAC8_SetSpeed
#define toneGen2_SetRange       toneGen2_VDAC8_SetRange
#define toneGen2_SetValue       toneGen2_VDAC8_SetValue
#define toneGen2_DacTrim        toneGen2_VDAC8_DacTrim
#define toneGen2_SaveConfig     toneGen2_VDAC8_SaveConfig
#define toneGen2_RestoreConfig  toneGen2_VDAC8_RestoreConfig


/***************************************
*    Variable with external linkage 
***************************************/

extern uint8 toneGen2_initVar;

extern const uint8 CYCODE toneGen2_wave1[toneGen2_WAVE1_LENGTH];
extern const uint8 CYCODE toneGen2_wave2[toneGen2_WAVE2_LENGTH];


/***************************************
*            API Constants
***************************************/

/* SetRange constants */
#if(toneGen2_DAC_MODE == toneGen2_VOLT_MODE)
    #define toneGen2_RANGE_1V       (0x00u)
    #define toneGen2_RANGE_4V       (0x04u)
#else /* current mode */
    #define toneGen2_RANGE_32uA     (0x00u)
    #define toneGen2_RANGE_255uA    (0x04u)
    #define toneGen2_RANGE_2mA      (0x08u)
    #define toneGen2_RANGE_2048uA   toneGen2_RANGE_2mA
#endif /* toneGen2_DAC_MODE == toneGen2_VOLT_MODE */

/* Power setting for SetSpeed API */
#define toneGen2_LOWSPEED       (0x00u)
#define toneGen2_HIGHSPEED      (0x02u)


/***************************************
*              Registers        
***************************************/

#define toneGen2_DAC8__D toneGen2_VDAC8_viDAC8__D


/***************************************
*         Register Constants       
***************************************/

/* CR0 vDac Control Register 0 definitions */

/* Bit Field  DAC_HS_MODE */
#define toneGen2_HS_MASK        (0x02u)
#define toneGen2_HS_LOWPOWER    (0x00u)
#define toneGen2_HS_HIGHSPEED   (0x02u)

/* Bit Field  DAC_MODE */
#define toneGen2_MODE_MASK      (0x10u)
#define toneGen2_MODE_V         (0x00u)
#define toneGen2_MODE_I         (0x10u)

/* Bit Field  DAC_RANGE */
#define toneGen2_RANGE_MASK     (0x0Cu)
#define toneGen2_RANGE_0        (0x00u)
#define toneGen2_RANGE_1        (0x04u)
#define toneGen2_RANGE_2        (0x08u)
#define toneGen2_RANGE_3        (0x0Cu)
#define toneGen2_IDIR_MASK      (0x04u)

#define toneGen2_DAC_RANGE      ((uint8)(1u << 2u) & toneGen2_RANGE_MASK)
#define toneGen2_DAC_POL        ((uint8)(1u >> 1u) & toneGen2_IDIR_MASK)


#endif /* CY_WaveDAC8_toneGen2_H  */

/* [] END OF FILE */
