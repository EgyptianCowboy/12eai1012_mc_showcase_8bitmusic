/*******************************************************************************
* File Name: toneGen1.h  
* Version 2.10
*
* Description:
*  This file contains the function prototypes and constants used in
*  the 8-bit Waveform DAC (WaveDAC8) Component.
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_WaveDAC8_toneGen1_H) 
#define CY_WaveDAC8_toneGen1_H

#include "cytypes.h"
#include "cyfitter.h"
#include <toneGen1_Wave1_DMA_dma.h>
#include <toneGen1_Wave2_DMA_dma.h>
#include <toneGen1_VDAC8.h>


/***************************************
*  Initial Parameter Constants
***************************************/

#define toneGen1_WAVE1_TYPE     (0u)     /* Waveform for wave1 */
#define toneGen1_WAVE2_TYPE     (2u)     /* Waveform for wave2 */
#define toneGen1_SINE_WAVE      (0u)
#define toneGen1_SQUARE_WAVE    (1u)
#define toneGen1_TRIANGLE_WAVE  (2u)
#define toneGen1_SAWTOOTH_WAVE  (3u)
#define toneGen1_ARB_DRAW_WAVE  (10u) /* Arbitrary (draw) */
#define toneGen1_ARB_FILE_WAVE  (11u) /* Arbitrary (from file) */

#define toneGen1_WAVE1_LENGTH   (100u)   /* Length for wave1 */
#define toneGen1_WAVE2_LENGTH   (100u)   /* Length for wave2 */
	
#define toneGen1_DEFAULT_RANGE    (1u) /* Default DAC range */
#define toneGen1_DAC_RANGE_1V     (0u)
#define toneGen1_DAC_RANGE_1V_BUF (16u)
#define toneGen1_DAC_RANGE_4V     (1u)
#define toneGen1_DAC_RANGE_4V_BUF (17u)
#define toneGen1_VOLT_MODE        (0u)
#define toneGen1_CURRENT_MODE     (1u)
#define toneGen1_DAC_MODE         (((toneGen1_DEFAULT_RANGE == toneGen1_DAC_RANGE_1V) || \
									  (toneGen1_DEFAULT_RANGE == toneGen1_DAC_RANGE_4V) || \
							  		  (toneGen1_DEFAULT_RANGE == toneGen1_DAC_RANGE_1V_BUF) || \
									  (toneGen1_DEFAULT_RANGE == toneGen1_DAC_RANGE_4V_BUF)) ? \
									   toneGen1_VOLT_MODE : toneGen1_CURRENT_MODE)

#define toneGen1_DACMODE toneGen1_DAC_MODE /* legacy definition for backward compatibility */

#define toneGen1_DIRECT_MODE (0u)
#define toneGen1_BUFFER_MODE (1u)
#define toneGen1_OUT_MODE    (((toneGen1_DEFAULT_RANGE == toneGen1_DAC_RANGE_1V_BUF) || \
								 (toneGen1_DEFAULT_RANGE == toneGen1_DAC_RANGE_4V_BUF)) ? \
								  toneGen1_BUFFER_MODE : toneGen1_DIRECT_MODE)

#if(toneGen1_OUT_MODE == toneGen1_BUFFER_MODE)
    #include <toneGen1_BuffAmp.h>
#endif /* toneGen1_OUT_MODE == toneGen1_BUFFER_MODE */

#define toneGen1_CLOCK_INT      (1u)
#define toneGen1_CLOCK_EXT      (0u)
#define toneGen1_CLOCK_SRC      (0u)

#if(toneGen1_CLOCK_SRC == toneGen1_CLOCK_INT)  
	#include <toneGen1_DacClk.h>
	#if defined(toneGen1_DacClk_PHASE)
		#define toneGen1_CLK_PHASE_0nS (1u)
	#endif /* defined(toneGen1_DacClk_PHASE) */
#endif /* toneGen1_CLOCK_SRC == toneGen1_CLOCK_INT */

#if (CY_PSOC3)
	#define toneGen1_HI16FLASHPTR   (0xFFu)
#endif /* CY_PSOC3 */

#define toneGen1_Wave1_DMA_BYTES_PER_BURST      (1u)
#define toneGen1_Wave1_DMA_REQUEST_PER_BURST    (1u)
#define toneGen1_Wave2_DMA_BYTES_PER_BURST      (1u)
#define toneGen1_Wave2_DMA_REQUEST_PER_BURST    (1u)


/***************************************
*   Data Struct Definition
***************************************/

/* Low power Mode API Support */
typedef struct
{
	uint8   enableState;
}toneGen1_BACKUP_STRUCT;


/***************************************
*        Function Prototypes 
***************************************/

void toneGen1_Start(void)             ;
void toneGen1_StartEx(const uint8 * wavePtr1, uint16 sampleSize1, const uint8 * wavePtr2, uint16 sampleSize2)
                                        ;
void toneGen1_Init(void)              ;
void toneGen1_Enable(void)            ;
void toneGen1_Stop(void)              ;

void toneGen1_Wave1Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;
void toneGen1_Wave2Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;

void toneGen1_Sleep(void)             ;
void toneGen1_Wakeup(void)            ;

#define toneGen1_SetSpeed       toneGen1_VDAC8_SetSpeed
#define toneGen1_SetRange       toneGen1_VDAC8_SetRange
#define toneGen1_SetValue       toneGen1_VDAC8_SetValue
#define toneGen1_DacTrim        toneGen1_VDAC8_DacTrim
#define toneGen1_SaveConfig     toneGen1_VDAC8_SaveConfig
#define toneGen1_RestoreConfig  toneGen1_VDAC8_RestoreConfig


/***************************************
*    Variable with external linkage 
***************************************/

extern uint8 toneGen1_initVar;

extern const uint8 CYCODE toneGen1_wave1[toneGen1_WAVE1_LENGTH];
extern const uint8 CYCODE toneGen1_wave2[toneGen1_WAVE2_LENGTH];


/***************************************
*            API Constants
***************************************/

/* SetRange constants */
#if(toneGen1_DAC_MODE == toneGen1_VOLT_MODE)
    #define toneGen1_RANGE_1V       (0x00u)
    #define toneGen1_RANGE_4V       (0x04u)
#else /* current mode */
    #define toneGen1_RANGE_32uA     (0x00u)
    #define toneGen1_RANGE_255uA    (0x04u)
    #define toneGen1_RANGE_2mA      (0x08u)
    #define toneGen1_RANGE_2048uA   toneGen1_RANGE_2mA
#endif /* toneGen1_DAC_MODE == toneGen1_VOLT_MODE */

/* Power setting for SetSpeed API */
#define toneGen1_LOWSPEED       (0x00u)
#define toneGen1_HIGHSPEED      (0x02u)


/***************************************
*              Registers        
***************************************/

#define toneGen1_DAC8__D toneGen1_VDAC8_viDAC8__D


/***************************************
*         Register Constants       
***************************************/

/* CR0 vDac Control Register 0 definitions */

/* Bit Field  DAC_HS_MODE */
#define toneGen1_HS_MASK        (0x02u)
#define toneGen1_HS_LOWPOWER    (0x00u)
#define toneGen1_HS_HIGHSPEED   (0x02u)

/* Bit Field  DAC_MODE */
#define toneGen1_MODE_MASK      (0x10u)
#define toneGen1_MODE_V         (0x00u)
#define toneGen1_MODE_I         (0x10u)

/* Bit Field  DAC_RANGE */
#define toneGen1_RANGE_MASK     (0x0Cu)
#define toneGen1_RANGE_0        (0x00u)
#define toneGen1_RANGE_1        (0x04u)
#define toneGen1_RANGE_2        (0x08u)
#define toneGen1_RANGE_3        (0x0Cu)
#define toneGen1_IDIR_MASK      (0x04u)

#define toneGen1_DAC_RANGE      ((uint8)(1u << 2u) & toneGen1_RANGE_MASK)
#define toneGen1_DAC_POL        ((uint8)(1u >> 1u) & toneGen1_IDIR_MASK)


#endif /* CY_WaveDAC8_toneGen1_H  */

/* [] END OF FILE */
