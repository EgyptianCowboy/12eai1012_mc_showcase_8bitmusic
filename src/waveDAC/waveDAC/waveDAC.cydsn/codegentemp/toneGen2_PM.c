/*******************************************************************************
* File Name: toneGen2_PM.c  
* Version 2.10
*
* Description:
*  This file provides the power manager source code to the API for 
*  the WaveDAC8 component.
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "toneGen2.h"

static toneGen2_BACKUP_STRUCT  toneGen2_backup;


/*******************************************************************************
* Function Name: toneGen2_Sleep
********************************************************************************
*
* Summary:
*  Stops the component and saves its configuration. Should be called 
*  just prior to entering sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  toneGen2_backup:  The structure field 'enableState' is modified 
*  depending on the enable state of the block before entering to sleep mode.
*
* Reentrant:
*  No
*
*******************************************************************************/
void toneGen2_Sleep(void) 
{
	/* Save DAC8's enable state */

	toneGen2_backup.enableState = (toneGen2_VDAC8_ACT_PWR_EN == 
		(toneGen2_VDAC8_PWRMGR_REG & toneGen2_VDAC8_ACT_PWR_EN)) ? 1u : 0u ;
	
	toneGen2_Stop();
	toneGen2_SaveConfig();
}


/*******************************************************************************
* Function Name: toneGen2_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component configuration. Should be called
*  just after awaking from sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  void
*
* Global variables:
*  toneGen2_backup:  The structure field 'enableState' is used to 
*  restore the enable state of block after wakeup from sleep mode.
*
* Reentrant:
*  No
*
*******************************************************************************/
void toneGen2_Wakeup(void) 
{
	toneGen2_RestoreConfig();

	if(toneGen2_backup.enableState == 1u)
	{
		toneGen2_Enable();
	}
}


/* [] END OF FILE */
