/*******************************************************************************
* File Name: toneGen1_PM.c  
* Version 2.10
*
* Description:
*  This file provides the power manager source code to the API for 
*  the WaveDAC8 component.
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "toneGen1.h"

static toneGen1_BACKUP_STRUCT  toneGen1_backup;


/*******************************************************************************
* Function Name: toneGen1_Sleep
********************************************************************************
*
* Summary:
*  Stops the component and saves its configuration. Should be called 
*  just prior to entering sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  toneGen1_backup:  The structure field 'enableState' is modified 
*  depending on the enable state of the block before entering to sleep mode.
*
* Reentrant:
*  No
*
*******************************************************************************/
void toneGen1_Sleep(void) 
{
	/* Save DAC8's enable state */

	toneGen1_backup.enableState = (toneGen1_VDAC8_ACT_PWR_EN == 
		(toneGen1_VDAC8_PWRMGR_REG & toneGen1_VDAC8_ACT_PWR_EN)) ? 1u : 0u ;
	
	toneGen1_Stop();
	toneGen1_SaveConfig();
}


/*******************************************************************************
* Function Name: toneGen1_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component configuration. Should be called
*  just after awaking from sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  void
*
* Global variables:
*  toneGen1_backup:  The structure field 'enableState' is used to 
*  restore the enable state of block after wakeup from sleep mode.
*
* Reentrant:
*  No
*
*******************************************************************************/
void toneGen1_Wakeup(void) 
{
	toneGen1_RestoreConfig();

	if(toneGen1_backup.enableState == 1u)
	{
		toneGen1_Enable();
	}
}


/* [] END OF FILE */
