/*******************************************************************************
* File Name: toneGen2_VDAC8_PM.c  
* Version 1.90
*
* Description:
*  This file provides the power management source code to API for the
*  VDAC8.  
*
* Note:
*  None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "toneGen2_VDAC8.h"

static toneGen2_VDAC8_backupStruct toneGen2_VDAC8_backup;


/*******************************************************************************
* Function Name: toneGen2_VDAC8_SaveConfig
********************************************************************************
* Summary:
*  Save the current user configuration
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void toneGen2_VDAC8_SaveConfig(void) 
{
    if (!((toneGen2_VDAC8_CR1 & toneGen2_VDAC8_SRC_MASK) == toneGen2_VDAC8_SRC_UDB))
    {
        toneGen2_VDAC8_backup.data_value = toneGen2_VDAC8_Data;
    }
}


/*******************************************************************************
* Function Name: toneGen2_VDAC8_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
*******************************************************************************/
void toneGen2_VDAC8_RestoreConfig(void) 
{
    if (!((toneGen2_VDAC8_CR1 & toneGen2_VDAC8_SRC_MASK) == toneGen2_VDAC8_SRC_UDB))
    {
        if((toneGen2_VDAC8_Strobe & toneGen2_VDAC8_STRB_MASK) == toneGen2_VDAC8_STRB_EN)
        {
            toneGen2_VDAC8_Strobe &= (uint8)(~toneGen2_VDAC8_STRB_MASK);
            toneGen2_VDAC8_Data = toneGen2_VDAC8_backup.data_value;
            toneGen2_VDAC8_Strobe |= toneGen2_VDAC8_STRB_EN;
        }
        else
        {
            toneGen2_VDAC8_Data = toneGen2_VDAC8_backup.data_value;
        }
    }
}


/*******************************************************************************
* Function Name: toneGen2_VDAC8_Sleep
********************************************************************************
* Summary:
*  Stop and Save the user configuration
*
* Parameters:  
*  void:  
*
* Return: 
*  void
*
* Global variables:
*  toneGen2_VDAC8_backup.enableState:  Is modified depending on the enable 
*  state  of the block before entering sleep mode.
*
*******************************************************************************/
void toneGen2_VDAC8_Sleep(void) 
{
    /* Save VDAC8's enable state */    
    if(toneGen2_VDAC8_ACT_PWR_EN == (toneGen2_VDAC8_PWRMGR & toneGen2_VDAC8_ACT_PWR_EN))
    {
        /* VDAC8 is enabled */
        toneGen2_VDAC8_backup.enableState = 1u;
    }
    else
    {
        /* VDAC8 is disabled */
        toneGen2_VDAC8_backup.enableState = 0u;
    }
    
    toneGen2_VDAC8_Stop();
    toneGen2_VDAC8_SaveConfig();
}


/*******************************************************************************
* Function Name: toneGen2_VDAC8_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  toneGen2_VDAC8_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void toneGen2_VDAC8_Wakeup(void) 
{
    toneGen2_VDAC8_RestoreConfig();
    
    if(toneGen2_VDAC8_backup.enableState == 1u)
    {
        /* Enable VDAC8's operation */
        toneGen2_VDAC8_Enable();

        /* Restore the data register */
        toneGen2_VDAC8_SetValue(toneGen2_VDAC8_Data);
    } /* Do nothing if VDAC8 was disabled before */    
}


/* [] END OF FILE */
