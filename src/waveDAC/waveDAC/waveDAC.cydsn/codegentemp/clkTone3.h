/*******************************************************************************
* File Name: clkTone3.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_clkTone3_H)
#define CY_CLOCK_clkTone3_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void clkTone3_Start(void) ;
void clkTone3_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void clkTone3_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void clkTone3_StandbyPower(uint8 state) ;
void clkTone3_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 clkTone3_GetDividerRegister(void) ;
void clkTone3_SetModeRegister(uint8 modeBitMask) ;
void clkTone3_ClearModeRegister(uint8 modeBitMask) ;
uint8 clkTone3_GetModeRegister(void) ;
void clkTone3_SetSourceRegister(uint8 clkSource) ;
uint8 clkTone3_GetSourceRegister(void) ;
#if defined(clkTone3__CFG3)
void clkTone3_SetPhaseRegister(uint8 clkPhase) ;
uint8 clkTone3_GetPhaseRegister(void) ;
#endif /* defined(clkTone3__CFG3) */

#define clkTone3_Enable()                       clkTone3_Start()
#define clkTone3_Disable()                      clkTone3_Stop()
#define clkTone3_SetDivider(clkDivider)         clkTone3_SetDividerRegister(clkDivider, 1u)
#define clkTone3_SetDividerValue(clkDivider)    clkTone3_SetDividerRegister((clkDivider) - 1u, 1u)
#define clkTone3_SetMode(clkMode)               clkTone3_SetModeRegister(clkMode)
#define clkTone3_SetSource(clkSource)           clkTone3_SetSourceRegister(clkSource)
#if defined(clkTone3__CFG3)
#define clkTone3_SetPhase(clkPhase)             clkTone3_SetPhaseRegister(clkPhase)
#define clkTone3_SetPhaseValue(clkPhase)        clkTone3_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(clkTone3__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define clkTone3_CLKEN              (* (reg8 *) clkTone3__PM_ACT_CFG)
#define clkTone3_CLKEN_PTR          ((reg8 *) clkTone3__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define clkTone3_CLKSTBY            (* (reg8 *) clkTone3__PM_STBY_CFG)
#define clkTone3_CLKSTBY_PTR        ((reg8 *) clkTone3__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define clkTone3_DIV_LSB            (* (reg8 *) clkTone3__CFG0)
#define clkTone3_DIV_LSB_PTR        ((reg8 *) clkTone3__CFG0)
#define clkTone3_DIV_PTR            ((reg16 *) clkTone3__CFG0)

/* Clock MSB divider configuration register. */
#define clkTone3_DIV_MSB            (* (reg8 *) clkTone3__CFG1)
#define clkTone3_DIV_MSB_PTR        ((reg8 *) clkTone3__CFG1)

/* Mode and source configuration register */
#define clkTone3_MOD_SRC            (* (reg8 *) clkTone3__CFG2)
#define clkTone3_MOD_SRC_PTR        ((reg8 *) clkTone3__CFG2)

#if defined(clkTone3__CFG3)
/* Analog clock phase configuration register */
#define clkTone3_PHASE              (* (reg8 *) clkTone3__CFG3)
#define clkTone3_PHASE_PTR          ((reg8 *) clkTone3__CFG3)
#endif /* defined(clkTone3__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define clkTone3_CLKEN_MASK         clkTone3__PM_ACT_MSK
#define clkTone3_CLKSTBY_MASK       clkTone3__PM_STBY_MSK

/* CFG2 field masks */
#define clkTone3_SRC_SEL_MSK        clkTone3__CFG2_SRC_SEL_MASK
#define clkTone3_MODE_MASK          (~(clkTone3_SRC_SEL_MSK))

#if defined(clkTone3__CFG3)
/* CFG3 phase mask */
#define clkTone3_PHASE_MASK         clkTone3__CFG3_PHASE_DLY_MASK
#endif /* defined(clkTone3__CFG3) */

#endif /* CY_CLOCK_clkTone3_H */


/* [] END OF FILE */
