/*******************************************************************************
* File Name: clkTone2.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_clkTone2_H)
#define CY_CLOCK_clkTone2_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void clkTone2_Start(void) ;
void clkTone2_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void clkTone2_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void clkTone2_StandbyPower(uint8 state) ;
void clkTone2_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 clkTone2_GetDividerRegister(void) ;
void clkTone2_SetModeRegister(uint8 modeBitMask) ;
void clkTone2_ClearModeRegister(uint8 modeBitMask) ;
uint8 clkTone2_GetModeRegister(void) ;
void clkTone2_SetSourceRegister(uint8 clkSource) ;
uint8 clkTone2_GetSourceRegister(void) ;
#if defined(clkTone2__CFG3)
void clkTone2_SetPhaseRegister(uint8 clkPhase) ;
uint8 clkTone2_GetPhaseRegister(void) ;
#endif /* defined(clkTone2__CFG3) */

#define clkTone2_Enable()                       clkTone2_Start()
#define clkTone2_Disable()                      clkTone2_Stop()
#define clkTone2_SetDivider(clkDivider)         clkTone2_SetDividerRegister(clkDivider, 1u)
#define clkTone2_SetDividerValue(clkDivider)    clkTone2_SetDividerRegister((clkDivider) - 1u, 1u)
#define clkTone2_SetMode(clkMode)               clkTone2_SetModeRegister(clkMode)
#define clkTone2_SetSource(clkSource)           clkTone2_SetSourceRegister(clkSource)
#if defined(clkTone2__CFG3)
#define clkTone2_SetPhase(clkPhase)             clkTone2_SetPhaseRegister(clkPhase)
#define clkTone2_SetPhaseValue(clkPhase)        clkTone2_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(clkTone2__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define clkTone2_CLKEN              (* (reg8 *) clkTone2__PM_ACT_CFG)
#define clkTone2_CLKEN_PTR          ((reg8 *) clkTone2__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define clkTone2_CLKSTBY            (* (reg8 *) clkTone2__PM_STBY_CFG)
#define clkTone2_CLKSTBY_PTR        ((reg8 *) clkTone2__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define clkTone2_DIV_LSB            (* (reg8 *) clkTone2__CFG0)
#define clkTone2_DIV_LSB_PTR        ((reg8 *) clkTone2__CFG0)
#define clkTone2_DIV_PTR            ((reg16 *) clkTone2__CFG0)

/* Clock MSB divider configuration register. */
#define clkTone2_DIV_MSB            (* (reg8 *) clkTone2__CFG1)
#define clkTone2_DIV_MSB_PTR        ((reg8 *) clkTone2__CFG1)

/* Mode and source configuration register */
#define clkTone2_MOD_SRC            (* (reg8 *) clkTone2__CFG2)
#define clkTone2_MOD_SRC_PTR        ((reg8 *) clkTone2__CFG2)

#if defined(clkTone2__CFG3)
/* Analog clock phase configuration register */
#define clkTone2_PHASE              (* (reg8 *) clkTone2__CFG3)
#define clkTone2_PHASE_PTR          ((reg8 *) clkTone2__CFG3)
#endif /* defined(clkTone2__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define clkTone2_CLKEN_MASK         clkTone2__PM_ACT_MSK
#define clkTone2_CLKSTBY_MASK       clkTone2__PM_STBY_MSK

/* CFG2 field masks */
#define clkTone2_SRC_SEL_MSK        clkTone2__CFG2_SRC_SEL_MASK
#define clkTone2_MODE_MASK          (~(clkTone2_SRC_SEL_MSK))

#if defined(clkTone2__CFG3)
/* CFG3 phase mask */
#define clkTone2_PHASE_MASK         clkTone2__CFG3_PHASE_DLY_MASK
#endif /* defined(clkTone2__CFG3) */

#endif /* CY_CLOCK_clkTone2_H */


/* [] END OF FILE */
