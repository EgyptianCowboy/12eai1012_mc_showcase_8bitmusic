/*******************************************************************************
* File Name: outputMux.c
* Version 1.80
*
*  Description:
*    This file contains all functions required for the analog multiplexer
*    AMux User Module.
*
*   Note:
*
*******************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "outputMux.h"

static uint8 outputMux_lastChannel = outputMux_NULL_CHANNEL;


/*******************************************************************************
* Function Name: outputMux_Start
********************************************************************************
* Summary:
*  Disconnect all channels.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void outputMux_Start(void) 
{
    uint8 chan;

    for(chan = 0u; chan < outputMux_CHANNELS ; chan++)
    {
#if (outputMux_MUXTYPE == outputMux_MUX_SINGLE)
        outputMux_Unset(chan);
#else
        outputMux_CYAMUXSIDE_A_Unset(chan);
        outputMux_CYAMUXSIDE_B_Unset(chan);
#endif
    }

    outputMux_lastChannel = outputMux_NULL_CHANNEL;
}


#if (!outputMux_ATMOSTONE)
/*******************************************************************************
* Function Name: outputMux_Select
********************************************************************************
* Summary:
*  This functions first disconnects all channels then connects the given
*  channel.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void outputMux_Select(uint8 channel) 
{
    outputMux_DisconnectAll();        /* Disconnect all previous connections */
    outputMux_Connect(channel);       /* Make the given selection */
    outputMux_lastChannel = channel;  /* Update last channel */
}
#endif


/*******************************************************************************
* Function Name: outputMux_FastSelect
********************************************************************************
* Summary:
*  This function first disconnects the last connection made with FastSelect or
*  Select, then connects the given channel. The FastSelect function is similar
*  to the Select function, except it is faster since it only disconnects the
*  last channel selected rather than all channels.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void outputMux_FastSelect(uint8 channel) 
{
    /* Disconnect the last valid channel */
    if( outputMux_lastChannel != outputMux_NULL_CHANNEL)
    {
        outputMux_Disconnect(outputMux_lastChannel);
    }

    /* Make the new channel connection */
#if (outputMux_MUXTYPE == outputMux_MUX_SINGLE)
    outputMux_Set(channel);
#else
    outputMux_CYAMUXSIDE_A_Set(channel);
    outputMux_CYAMUXSIDE_B_Set(channel);
#endif


    outputMux_lastChannel = channel;   /* Update last channel */
}


#if (outputMux_MUXTYPE == outputMux_MUX_DIFF)
#if (!outputMux_ATMOSTONE)
/*******************************************************************************
* Function Name: outputMux_Connect
********************************************************************************
* Summary:
*  This function connects the given channel without affecting other connections.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void outputMux_Connect(uint8 channel) 
{
    outputMux_CYAMUXSIDE_A_Set(channel);
    outputMux_CYAMUXSIDE_B_Set(channel);
}
#endif

/*******************************************************************************
* Function Name: outputMux_Disconnect
********************************************************************************
* Summary:
*  This function disconnects the given channel from the common or output
*  terminal without affecting other connections.
*
* Parameters:
*  channel:  The channel to disconnect from the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void outputMux_Disconnect(uint8 channel) 
{
    outputMux_CYAMUXSIDE_A_Unset(channel);
    outputMux_CYAMUXSIDE_B_Unset(channel);
}
#endif

#if (outputMux_ATMOSTONE)
/*******************************************************************************
* Function Name: outputMux_DisconnectAll
********************************************************************************
* Summary:
*  This function disconnects all channels.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void outputMux_DisconnectAll(void) 
{
    if(outputMux_lastChannel != outputMux_NULL_CHANNEL) 
    {
        outputMux_Disconnect(outputMux_lastChannel);
        outputMux_lastChannel = outputMux_NULL_CHANNEL;
    }
}
#endif

/* [] END OF FILE */
