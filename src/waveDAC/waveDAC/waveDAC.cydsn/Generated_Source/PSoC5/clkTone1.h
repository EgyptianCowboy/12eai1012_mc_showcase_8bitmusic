/*******************************************************************************
* File Name: clkTone1.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_clkTone1_H)
#define CY_CLOCK_clkTone1_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void clkTone1_Start(void) ;
void clkTone1_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void clkTone1_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void clkTone1_StandbyPower(uint8 state) ;
void clkTone1_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 clkTone1_GetDividerRegister(void) ;
void clkTone1_SetModeRegister(uint8 modeBitMask) ;
void clkTone1_ClearModeRegister(uint8 modeBitMask) ;
uint8 clkTone1_GetModeRegister(void) ;
void clkTone1_SetSourceRegister(uint8 clkSource) ;
uint8 clkTone1_GetSourceRegister(void) ;
#if defined(clkTone1__CFG3)
void clkTone1_SetPhaseRegister(uint8 clkPhase) ;
uint8 clkTone1_GetPhaseRegister(void) ;
#endif /* defined(clkTone1__CFG3) */

#define clkTone1_Enable()                       clkTone1_Start()
#define clkTone1_Disable()                      clkTone1_Stop()
#define clkTone1_SetDivider(clkDivider)         clkTone1_SetDividerRegister(clkDivider, 1u)
#define clkTone1_SetDividerValue(clkDivider)    clkTone1_SetDividerRegister((clkDivider) - 1u, 1u)
#define clkTone1_SetMode(clkMode)               clkTone1_SetModeRegister(clkMode)
#define clkTone1_SetSource(clkSource)           clkTone1_SetSourceRegister(clkSource)
#if defined(clkTone1__CFG3)
#define clkTone1_SetPhase(clkPhase)             clkTone1_SetPhaseRegister(clkPhase)
#define clkTone1_SetPhaseValue(clkPhase)        clkTone1_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(clkTone1__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define clkTone1_CLKEN              (* (reg8 *) clkTone1__PM_ACT_CFG)
#define clkTone1_CLKEN_PTR          ((reg8 *) clkTone1__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define clkTone1_CLKSTBY            (* (reg8 *) clkTone1__PM_STBY_CFG)
#define clkTone1_CLKSTBY_PTR        ((reg8 *) clkTone1__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define clkTone1_DIV_LSB            (* (reg8 *) clkTone1__CFG0)
#define clkTone1_DIV_LSB_PTR        ((reg8 *) clkTone1__CFG0)
#define clkTone1_DIV_PTR            ((reg16 *) clkTone1__CFG0)

/* Clock MSB divider configuration register. */
#define clkTone1_DIV_MSB            (* (reg8 *) clkTone1__CFG1)
#define clkTone1_DIV_MSB_PTR        ((reg8 *) clkTone1__CFG1)

/* Mode and source configuration register */
#define clkTone1_MOD_SRC            (* (reg8 *) clkTone1__CFG2)
#define clkTone1_MOD_SRC_PTR        ((reg8 *) clkTone1__CFG2)

#if defined(clkTone1__CFG3)
/* Analog clock phase configuration register */
#define clkTone1_PHASE              (* (reg8 *) clkTone1__CFG3)
#define clkTone1_PHASE_PTR          ((reg8 *) clkTone1__CFG3)
#endif /* defined(clkTone1__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define clkTone1_CLKEN_MASK         clkTone1__PM_ACT_MSK
#define clkTone1_CLKSTBY_MASK       clkTone1__PM_STBY_MSK

/* CFG2 field masks */
#define clkTone1_SRC_SEL_MSK        clkTone1__CFG2_SRC_SEL_MASK
#define clkTone1_MODE_MASK          (~(clkTone1_SRC_SEL_MSK))

#if defined(clkTone1__CFG3)
/* CFG3 phase mask */
#define clkTone1_PHASE_MASK         clkTone1__CFG3_PHASE_DLY_MASK
#endif /* defined(clkTone1__CFG3) */

#endif /* CY_CLOCK_clkTone1_H */


/* [] END OF FILE */
