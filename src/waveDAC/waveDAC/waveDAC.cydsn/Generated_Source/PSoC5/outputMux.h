/*******************************************************************************
* File Name: outputMux.h
* Version 1.80
*
*  Description:
*    This file contains the constants and function prototypes for the Analog
*    Multiplexer User Module AMux.
*
*   Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_AMUX_outputMux_H)
#define CY_AMUX_outputMux_H

#include "cyfitter.h"
#include "cyfitter_cfg.h"

#if ((CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) || \
         (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC4) || \
         (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC5))    
    #include "cytypes.h"
#else
    #include "syslib/cy_syslib.h"
#endif /* ((CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) */


/***************************************
*        Function Prototypes
***************************************/

void outputMux_Start(void) ;
#define outputMux_Init() outputMux_Start()
void outputMux_FastSelect(uint8 channel) ;
/* The Stop, Select, Connect, Disconnect and DisconnectAll functions are declared elsewhere */
/* void outputMux_Stop(void); */
/* void outputMux_Select(uint8 channel); */
/* void outputMux_Connect(uint8 channel); */
/* void outputMux_Disconnect(uint8 channel); */
/* void outputMux_DisconnectAll(void) */


/***************************************
*         Parameter Constants
***************************************/

#define outputMux_CHANNELS  3u
#define outputMux_MUXTYPE   1
#define outputMux_ATMOSTONE 0

/***************************************
*             API Constants
***************************************/

#define outputMux_NULL_CHANNEL 0xFFu
#define outputMux_MUX_SINGLE   1
#define outputMux_MUX_DIFF     2


/***************************************
*        Conditional Functions
***************************************/

#if outputMux_MUXTYPE == outputMux_MUX_SINGLE
# if !outputMux_ATMOSTONE
#  define outputMux_Connect(channel) outputMux_Set(channel)
# endif
# define outputMux_Disconnect(channel) outputMux_Unset(channel)
#else
# if !outputMux_ATMOSTONE
void outputMux_Connect(uint8 channel) ;
# endif
void outputMux_Disconnect(uint8 channel) ;
#endif

#if outputMux_ATMOSTONE
# define outputMux_Stop() outputMux_DisconnectAll()
# define outputMux_Select(channel) outputMux_FastSelect(channel)
void outputMux_DisconnectAll(void) ;
#else
# define outputMux_Stop() outputMux_Start()
void outputMux_Select(uint8 channel) ;
# define outputMux_DisconnectAll() outputMux_Start()
#endif

#endif /* CY_AMUX_outputMux_H */


/* [] END OF FILE */
