/*******************************************************************************
* File Name: waveSelect_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "waveSelect.h"

/* Check for removal by optimization */
#if !defined(waveSelect_Sync_ctrl_reg__REMOVED)

static waveSelect_BACKUP_STRUCT  waveSelect_backup = {0u};

    
/*******************************************************************************
* Function Name: waveSelect_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void waveSelect_SaveConfig(void) 
{
    waveSelect_backup.controlState = waveSelect_Control;
}


/*******************************************************************************
* Function Name: waveSelect_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void waveSelect_RestoreConfig(void) 
{
     waveSelect_Control = waveSelect_backup.controlState;
}


/*******************************************************************************
* Function Name: waveSelect_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void waveSelect_Sleep(void) 
{
    waveSelect_SaveConfig();
}


/*******************************************************************************
* Function Name: waveSelect_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void waveSelect_Wakeup(void)  
{
    waveSelect_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
