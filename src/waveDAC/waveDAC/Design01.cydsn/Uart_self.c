/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef UART_SELF
    #define UART_SELF
    #include "Uart_self.h"
    
    /*void setData( int *strData, threeTone *dataUse )
    {
        dataUse->tone1.toneFreq = strData[0];
        dataUse->tone1.DT = strData[1];
        dataUse->tone1.ctrlReg = strData[2];
        dataUse->tone2.toneFreq = strData[3];
        dataUse->tone2.DT = strData[4];
        dataUse->tone2.ctrlReg = strData[5];
        dataUse->tone3.toneFreq = strData[6];
        dataUse->tone3.DT = strData[7];
        dataUse->tone3.ctrlReg = strData[8];
        dataUse->vol = strData[9];
        dataUse->delay = strData[10];
    }*/
    
    void collectData( uint8 rx, int *strData, volatile int a, volatile int b )
    {
        if(a == 0)
        {
            strData[b] = 0;
        }
        strData[b] *= 10;
        strData[b] += ((int) rx - 48);
    }
    
#endif
    
/* [] END OF FILE */
