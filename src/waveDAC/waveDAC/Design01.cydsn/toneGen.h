/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include <stdlib.h>
#include <math.h>

#define CLK_FREQ 24000000

typedef struct toneArr_t {
    uint8 triArray[255];
    //uint8 sawArray[255];
    uint8 sawArray[255];
    uint8 noiseArray[255];
    uint8 pulse1Array[255];
    uint8 pulse2Array[255];
    uint8 pulse3Array[255];
    uint16 arrSize;
} toneArr;

typedef struct clkParam_t {
    uint16 clk1Div;
    uint16 clk2Div;
    uint16 clk3Div;
} clkParam;

typedef struct pulseDtCnt_t {
    uint16 pulse1DtCnt;
    uint16 pulse2DtCnt;
    uint16 pulse3DtCnt;
} pulseDtCnt;

void initToneGen();
void startTones(uint8 resetBit);
void stopTones();
void fillSaw(uint8* sawArr, uint16 length);
void fillTri(uint8* triArr, uint16 length);
//void fillArrays(toneArr* localTones, uint16 length);
void fillNoise(uint8* noiseArr, size_t length);
void fillPulse(uint8* pulseArr, size_t length, uint8 dtCnt);
uint16 setDtCnt(uint8 DT);
uint16 clockDiv(uint16 desFreq);

/* [] END OF FILE */
