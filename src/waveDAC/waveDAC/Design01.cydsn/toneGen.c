/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef TONEGEN
    #define TONEGEN
    #include <math.h>
    #include "toneGen.h"

    void initToneGen() {
        //waveSelect_Control = 0;
        outputOpamp_Start();
        toneGen1_Start();
        toneGen2_Start();
        toneGen3_Start();
        
    }
    
    void startTones(uint8 resetBit) {
        if(resetBit & 1)
            toneGen1_Start();
        if(resetBit & 2)
            toneGen2_Start();
        if(resetBit & 4)
            toneGen3_Start();
    }
    
    void stopTones() {
        toneGen1_Stop();
        toneGen2_Stop();
        toneGen3_Stop();
    }
    
    void fillSaw(uint8* sawArr, uint16 length) {
        for(uint16 i=0; i<length; i++) {
            sawArr[i] = i;
        }
    }
    
    void fillTri(uint8* triArr, uint16 length) {
        /*for(int16 i = 0; i<50; i++) {
            triArr[i] = 0;   
        }*/
        for(int16 i=0; i<(length/2)+1; i++) {
            triArr[i] = 2*i;
        }
        for(int16 i=length/2; i>=0; i--) {
            triArr[127+(127-i)] = 2*i;
        }
    }
    
//    void fillArrays(toneArr* localTones, uint16 length) {
//        for(int16 i=0; i<length/2+1; i++) {
//            localTones->triArray[i] = 2*i;
//        }
//        for(int16 i=length; i>=0; i--) {
//            localTones->triArray[127+(127-i)] = 2*i;
//        }
//        for(int16 i=0; i<length; i++) {
//            localTones->sawArray[i] = i;
//        }
//    }
    
    void fillNoise(uint8* noiseArr, size_t length) {
        for(uint16 i=0; i<length; i++) {
            noiseArr[i] = rand();
        }
    }
    
    void fillPulse(uint8* pulseArr, size_t length, uint8 dtCnt) {
        for(uint16 i=0; i<dtCnt; i++) {
            pulseArr[i] = 255;
        }
        for(uint16 i=dtCnt; i<length; i++) {
            pulseArr[i] = 0;
        }  
    }
    
    uint16 setDtCnt(uint8 DT) {
        return (255/100) * (DT * 1.30);
        //return ceil((255/100)) * (DT * 1.30);
    }
    
    uint16 clockDiv(uint16 desFreq) {
        return CLK_FREQ/desFreq;
        //return ceil(CLK_FREQ/desFreq);
    }
#endif
/* [] END OF FILE */
