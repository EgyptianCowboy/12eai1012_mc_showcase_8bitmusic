/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE->
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company->
 *
 * ========================================
*/
#include "toneGen.h"
#include "Uart_self.h"

#define TONESAMPLESIZE 255

volatile uint8 rxData;
int str[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
volatile int i = 0, t = 0;
toneArr localTones;

threeTone data;

CY_ISR(UART_ISR)
{
    rxData = UART_GetChar();
    if(rxData)
    {
        if(rxData == 'A')
        {
            i++;
            if(i == 11)
            {
                setData( str, &data );
                
                stopTones();
    
                if(data.tone1.ctrlReg & 1)
                {
                    toneGen1_Wave1Setup(localTones.noiseArray,localTones.arrSize);
                }
                else if(data.tone1.ctrlReg & 2)
                {
                    toneGen1_Wave1Setup(localTones.pulse1Array,localTones.arrSize);
                }
                else if(data.tone1.ctrlReg & 4)
                {
                    toneGen1_Wave1Setup(localTones.sawArray,localTones.arrSize);
                }
                else if(data.tone1.ctrlReg & 8)
                {
                    toneGen1_Wave1Setup(localTones.triArray,localTones.arrSize);
                }
                
                if(data.tone2.ctrlReg & 1)
                {
                    toneGen2_Wave1Setup(localTones.noiseArray,localTones.arrSize);
                }
                else if(data.tone2.ctrlReg & 2)
                {
                    toneGen2_Wave1Setup(localTones.pulse2Array,localTones.arrSize);
                }
                else if(data.tone2.ctrlReg & 4)
                {
                    toneGen2_Wave1Setup(localTones.sawArray,localTones.arrSize);
                }
                else if(data.tone2.ctrlReg & 8)
                {
                    toneGen2_Wave1Setup(localTones.triArray,localTones.arrSize);
                }
                
                if(data.tone3.ctrlReg & 1)
                {
                    toneGen3_Wave1Setup(localTones.noiseArray,localTones.arrSize);
                }
                else if(data.tone3.ctrlReg & 2)
                {
                    toneGen3_Wave1Setup(localTones.pulse3Array,localTones.arrSize);
                }
                else if(data.tone3.ctrlReg & 4)
                {
                    toneGen3_Wave1Setup(localTones.sawArray,localTones.arrSize);
                }
                else if(data.tone3.ctrlReg & 8)
                {
                    toneGen3_Wave1Setup(localTones.triArray,localTones.arrSize);
                }
                
                
                //toneGen1_Wave1Setup(localTones.triArray,localTones.arrSize);
                //toneGen2_Wave1Setup(localTones.pulse2Array,localTones.arrSize);
                //toneGen3_Wave1Setup(localTones.pulse3Array,localTones.arrSize);
                startTones(3);
                
                i = 0;
            }
            t = 0;
        }
        else
        {
            collectData( rxData, str, t, i );
            t++;
        }
    }
}

int main(void)
{
    isr_Uart_StartEx(UART_ISR);
    CyGlobalIntEnable; /* Enable global interrupts-> */
    /* Place your initialization/startup code here (e->g-> MyInst_Start()) */
    UART_Start();
    initToneGen();
    
    initData( &data );
    
    pulseDtCnt pulseCnts;
    clkParam clkParameters;
    clkParameters.clk1Div = clockDiv(data.tone1.toneFreq);
    clkParameters.clk2Div = clockDiv(data.tone2.toneFreq);
    clkParameters.clk3Div = clockDiv(data.tone3.toneFreq);
    pulseCnts.pulse1DtCnt = setDtCnt(data.tone1.DT);
    pulseCnts.pulse2DtCnt = setDtCnt(data.tone2.DT);
    pulseCnts.pulse3DtCnt = setDtCnt(data.tone3.DT);
    
    //localTones = malloc(sizeof(localTones));
    localTones.arrSize = sizeof(localTones.triArray);
    
    fillSaw((&localTones)->sawArray, (&localTones)->arrSize);
    fillTri((&localTones)->triArray, (&localTones)->arrSize);
    fillNoise((&localTones)->noiseArray, (&localTones)->arrSize);
    fillPulse((&localTones)->pulse1Array, (&localTones)->arrSize, pulseCnts.pulse1DtCnt);
    fillPulse((&localTones)->pulse2Array, (&localTones)->arrSize, pulseCnts.pulse2DtCnt);
    fillPulse((&localTones)->pulse3Array, (&localTones)->arrSize, pulseCnts.pulse3DtCnt);
    clkTone1_SetDivider(clkParameters.clk1Div);
    clkTone2_SetDivider(clkParameters.clk2Div);
    clkTone3_SetDivider(clkParameters.clk3Div);
    stopTones();
    
    for(;;)
    {   
        
        /* Place your application code here-> */
        
    }
}

/* [] END OF FILE */