/*******************************************************************************
* File Name: outputPin.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_outputPin_H) /* Pins outputPin_H */
#define CY_PINS_outputPin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "outputPin_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 outputPin__PORT == 15 && ((outputPin__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    outputPin_Write(uint8 value);
void    outputPin_SetDriveMode(uint8 mode);
uint8   outputPin_ReadDataReg(void);
uint8   outputPin_Read(void);
void    outputPin_SetInterruptMode(uint16 position, uint16 mode);
uint8   outputPin_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the outputPin_SetDriveMode() function.
     *  @{
     */
        #define outputPin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define outputPin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define outputPin_DM_RES_UP          PIN_DM_RES_UP
        #define outputPin_DM_RES_DWN         PIN_DM_RES_DWN
        #define outputPin_DM_OD_LO           PIN_DM_OD_LO
        #define outputPin_DM_OD_HI           PIN_DM_OD_HI
        #define outputPin_DM_STRONG          PIN_DM_STRONG
        #define outputPin_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define outputPin_MASK               outputPin__MASK
#define outputPin_SHIFT              outputPin__SHIFT
#define outputPin_WIDTH              1u

/* Interrupt constants */
#if defined(outputPin__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in outputPin_SetInterruptMode() function.
     *  @{
     */
        #define outputPin_INTR_NONE      (uint16)(0x0000u)
        #define outputPin_INTR_RISING    (uint16)(0x0001u)
        #define outputPin_INTR_FALLING   (uint16)(0x0002u)
        #define outputPin_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define outputPin_INTR_MASK      (0x01u) 
#endif /* (outputPin__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define outputPin_PS                     (* (reg8 *) outputPin__PS)
/* Data Register */
#define outputPin_DR                     (* (reg8 *) outputPin__DR)
/* Port Number */
#define outputPin_PRT_NUM                (* (reg8 *) outputPin__PRT) 
/* Connect to Analog Globals */                                                  
#define outputPin_AG                     (* (reg8 *) outputPin__AG)                       
/* Analog MUX bux enable */
#define outputPin_AMUX                   (* (reg8 *) outputPin__AMUX) 
/* Bidirectional Enable */                                                        
#define outputPin_BIE                    (* (reg8 *) outputPin__BIE)
/* Bit-mask for Aliased Register Access */
#define outputPin_BIT_MASK               (* (reg8 *) outputPin__BIT_MASK)
/* Bypass Enable */
#define outputPin_BYP                    (* (reg8 *) outputPin__BYP)
/* Port wide control signals */                                                   
#define outputPin_CTL                    (* (reg8 *) outputPin__CTL)
/* Drive Modes */
#define outputPin_DM0                    (* (reg8 *) outputPin__DM0) 
#define outputPin_DM1                    (* (reg8 *) outputPin__DM1)
#define outputPin_DM2                    (* (reg8 *) outputPin__DM2) 
/* Input Buffer Disable Override */
#define outputPin_INP_DIS                (* (reg8 *) outputPin__INP_DIS)
/* LCD Common or Segment Drive */
#define outputPin_LCD_COM_SEG            (* (reg8 *) outputPin__LCD_COM_SEG)
/* Enable Segment LCD */
#define outputPin_LCD_EN                 (* (reg8 *) outputPin__LCD_EN)
/* Slew Rate Control */
#define outputPin_SLW                    (* (reg8 *) outputPin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define outputPin_PRTDSI__CAPS_SEL       (* (reg8 *) outputPin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define outputPin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) outputPin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define outputPin_PRTDSI__OE_SEL0        (* (reg8 *) outputPin__PRTDSI__OE_SEL0) 
#define outputPin_PRTDSI__OE_SEL1        (* (reg8 *) outputPin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define outputPin_PRTDSI__OUT_SEL0       (* (reg8 *) outputPin__PRTDSI__OUT_SEL0) 
#define outputPin_PRTDSI__OUT_SEL1       (* (reg8 *) outputPin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define outputPin_PRTDSI__SYNC_OUT       (* (reg8 *) outputPin__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(outputPin__SIO_CFG)
    #define outputPin_SIO_HYST_EN        (* (reg8 *) outputPin__SIO_HYST_EN)
    #define outputPin_SIO_REG_HIFREQ     (* (reg8 *) outputPin__SIO_REG_HIFREQ)
    #define outputPin_SIO_CFG            (* (reg8 *) outputPin__SIO_CFG)
    #define outputPin_SIO_DIFF           (* (reg8 *) outputPin__SIO_DIFF)
#endif /* (outputPin__SIO_CFG) */

/* Interrupt Registers */
#if defined(outputPin__INTSTAT)
    #define outputPin_INTSTAT            (* (reg8 *) outputPin__INTSTAT)
    #define outputPin_SNAP               (* (reg8 *) outputPin__SNAP)
    
	#define outputPin_0_INTTYPE_REG 		(* (reg8 *) outputPin__0__INTTYPE)
#endif /* (outputPin__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_outputPin_H */


/* [] END OF FILE */
