/*******************************************************************************
* File Name: outputOpamp_PM.c
* Version 1.90
*
* Description:
*  This file provides the power management source code to the API for the 
*  OpAmp (Analog Buffer) component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "outputOpamp.h"

static outputOpamp_BACKUP_STRUCT  outputOpamp_backup;


/*******************************************************************************  
* Function Name: outputOpamp_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration registers.
* 
* Parameters:
*  void
* 
* Return:
*  void
*
*******************************************************************************/
void outputOpamp_SaveConfig(void) 
{
    /* Nothing to save as registers are System reset on retention flops */
}


/*******************************************************************************  
* Function Name: outputOpamp_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration registers.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void outputOpamp_RestoreConfig(void) 
{
    /* Nothing to restore */
}


/*******************************************************************************   
* Function Name: outputOpamp_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves its configuration. Should be called 
*  just prior to entering sleep.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  outputOpamp_backup: The structure field 'enableState' is modified 
*  depending on the enable state of the block before entering to sleep mode.
*
*******************************************************************************/
void outputOpamp_Sleep(void) 
{
    /* Save OpAmp enable state */
    if((outputOpamp_PM_ACT_CFG_REG & outputOpamp_ACT_PWR_EN) != 0u)
    {
        /* Component is enabled */
        outputOpamp_backup.enableState = 1u;
         /* Stops the component */
         outputOpamp_Stop();
    }
    else
    {
        /* Component is disabled */
        outputOpamp_backup.enableState = 0u;
    }
    /* Saves the configuration */
    outputOpamp_SaveConfig();
}


/*******************************************************************************  
* Function Name: outputOpamp_Wakeup
********************************************************************************
*
* Summary:
*  Enables block's operation and restores its configuration. Should be called
*  just after awaking from sleep.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  outputOpamp_backup: The structure field 'enableState' is used to 
*  restore the enable state of block after wakeup from sleep mode.
*
*******************************************************************************/
void outputOpamp_Wakeup(void) 
{
    /* Restore the user configuration */
    outputOpamp_RestoreConfig();

    /* Enables the component operation */
    if(outputOpamp_backup.enableState == 1u)
    {
        outputOpamp_Enable();
    } /* Do nothing if component was disable before */
}


/* [] END OF FILE */
