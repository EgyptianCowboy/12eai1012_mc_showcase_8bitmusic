/*******************************************************************************
* File Name: toneGen3.h  
* Version 2.10
*
* Description:
*  This file contains the function prototypes and constants used in
*  the 8-bit Waveform DAC (WaveDAC8) Component.
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_WaveDAC8_toneGen3_H) 
#define CY_WaveDAC8_toneGen3_H

#include "cytypes.h"
#include "cyfitter.h"
#include <toneGen3_Wave1_DMA_dma.h>
#include <toneGen3_Wave2_DMA_dma.h>
#include <toneGen3_VDAC8.h>


/***************************************
*  Initial Parameter Constants
***************************************/

#define toneGen3_WAVE1_TYPE     (0u)     /* Waveform for wave1 */
#define toneGen3_WAVE2_TYPE     (2u)     /* Waveform for wave2 */
#define toneGen3_SINE_WAVE      (0u)
#define toneGen3_SQUARE_WAVE    (1u)
#define toneGen3_TRIANGLE_WAVE  (2u)
#define toneGen3_SAWTOOTH_WAVE  (3u)
#define toneGen3_ARB_DRAW_WAVE  (10u) /* Arbitrary (draw) */
#define toneGen3_ARB_FILE_WAVE  (11u) /* Arbitrary (from file) */

#define toneGen3_WAVE1_LENGTH   (100u)   /* Length for wave1 */
#define toneGen3_WAVE2_LENGTH   (100u)   /* Length for wave2 */
	
#define toneGen3_DEFAULT_RANGE    (1u) /* Default DAC range */
#define toneGen3_DAC_RANGE_1V     (0u)
#define toneGen3_DAC_RANGE_1V_BUF (16u)
#define toneGen3_DAC_RANGE_4V     (1u)
#define toneGen3_DAC_RANGE_4V_BUF (17u)
#define toneGen3_VOLT_MODE        (0u)
#define toneGen3_CURRENT_MODE     (1u)
#define toneGen3_DAC_MODE         (((toneGen3_DEFAULT_RANGE == toneGen3_DAC_RANGE_1V) || \
									  (toneGen3_DEFAULT_RANGE == toneGen3_DAC_RANGE_4V) || \
							  		  (toneGen3_DEFAULT_RANGE == toneGen3_DAC_RANGE_1V_BUF) || \
									  (toneGen3_DEFAULT_RANGE == toneGen3_DAC_RANGE_4V_BUF)) ? \
									   toneGen3_VOLT_MODE : toneGen3_CURRENT_MODE)

#define toneGen3_DACMODE toneGen3_DAC_MODE /* legacy definition for backward compatibility */

#define toneGen3_DIRECT_MODE (0u)
#define toneGen3_BUFFER_MODE (1u)
#define toneGen3_OUT_MODE    (((toneGen3_DEFAULT_RANGE == toneGen3_DAC_RANGE_1V_BUF) || \
								 (toneGen3_DEFAULT_RANGE == toneGen3_DAC_RANGE_4V_BUF)) ? \
								  toneGen3_BUFFER_MODE : toneGen3_DIRECT_MODE)

#if(toneGen3_OUT_MODE == toneGen3_BUFFER_MODE)
    #include <toneGen3_BuffAmp.h>
#endif /* toneGen3_OUT_MODE == toneGen3_BUFFER_MODE */

#define toneGen3_CLOCK_INT      (1u)
#define toneGen3_CLOCK_EXT      (0u)
#define toneGen3_CLOCK_SRC      (0u)

#if(toneGen3_CLOCK_SRC == toneGen3_CLOCK_INT)  
	#include <toneGen3_DacClk.h>
	#if defined(toneGen3_DacClk_PHASE)
		#define toneGen3_CLK_PHASE_0nS (1u)
	#endif /* defined(toneGen3_DacClk_PHASE) */
#endif /* toneGen3_CLOCK_SRC == toneGen3_CLOCK_INT */

#if (CY_PSOC3)
	#define toneGen3_HI16FLASHPTR   (0xFFu)
#endif /* CY_PSOC3 */

#define toneGen3_Wave1_DMA_BYTES_PER_BURST      (1u)
#define toneGen3_Wave1_DMA_REQUEST_PER_BURST    (1u)
#define toneGen3_Wave2_DMA_BYTES_PER_BURST      (1u)
#define toneGen3_Wave2_DMA_REQUEST_PER_BURST    (1u)


/***************************************
*   Data Struct Definition
***************************************/

/* Low power Mode API Support */
typedef struct
{
	uint8   enableState;
}toneGen3_BACKUP_STRUCT;


/***************************************
*        Function Prototypes 
***************************************/

void toneGen3_Start(void)             ;
void toneGen3_StartEx(const uint8 * wavePtr1, uint16 sampleSize1, const uint8 * wavePtr2, uint16 sampleSize2)
                                        ;
void toneGen3_Init(void)              ;
void toneGen3_Enable(void)            ;
void toneGen3_Stop(void)              ;

void toneGen3_Wave1Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;
void toneGen3_Wave2Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;

void toneGen3_Sleep(void)             ;
void toneGen3_Wakeup(void)            ;

#define toneGen3_SetSpeed       toneGen3_VDAC8_SetSpeed
#define toneGen3_SetRange       toneGen3_VDAC8_SetRange
#define toneGen3_SetValue       toneGen3_VDAC8_SetValue
#define toneGen3_DacTrim        toneGen3_VDAC8_DacTrim
#define toneGen3_SaveConfig     toneGen3_VDAC8_SaveConfig
#define toneGen3_RestoreConfig  toneGen3_VDAC8_RestoreConfig


/***************************************
*    Variable with external linkage 
***************************************/

extern uint8 toneGen3_initVar;

extern const uint8 CYCODE toneGen3_wave1[toneGen3_WAVE1_LENGTH];
extern const uint8 CYCODE toneGen3_wave2[toneGen3_WAVE2_LENGTH];


/***************************************
*            API Constants
***************************************/

/* SetRange constants */
#if(toneGen3_DAC_MODE == toneGen3_VOLT_MODE)
    #define toneGen3_RANGE_1V       (0x00u)
    #define toneGen3_RANGE_4V       (0x04u)
#else /* current mode */
    #define toneGen3_RANGE_32uA     (0x00u)
    #define toneGen3_RANGE_255uA    (0x04u)
    #define toneGen3_RANGE_2mA      (0x08u)
    #define toneGen3_RANGE_2048uA   toneGen3_RANGE_2mA
#endif /* toneGen3_DAC_MODE == toneGen3_VOLT_MODE */

/* Power setting for SetSpeed API */
#define toneGen3_LOWSPEED       (0x00u)
#define toneGen3_HIGHSPEED      (0x02u)


/***************************************
*              Registers        
***************************************/

#define toneGen3_DAC8__D toneGen3_VDAC8_viDAC8__D


/***************************************
*         Register Constants       
***************************************/

/* CR0 vDac Control Register 0 definitions */

/* Bit Field  DAC_HS_MODE */
#define toneGen3_HS_MASK        (0x02u)
#define toneGen3_HS_LOWPOWER    (0x00u)
#define toneGen3_HS_HIGHSPEED   (0x02u)

/* Bit Field  DAC_MODE */
#define toneGen3_MODE_MASK      (0x10u)
#define toneGen3_MODE_V         (0x00u)
#define toneGen3_MODE_I         (0x10u)

/* Bit Field  DAC_RANGE */
#define toneGen3_RANGE_MASK     (0x0Cu)
#define toneGen3_RANGE_0        (0x00u)
#define toneGen3_RANGE_1        (0x04u)
#define toneGen3_RANGE_2        (0x08u)
#define toneGen3_RANGE_3        (0x0Cu)
#define toneGen3_IDIR_MASK      (0x04u)

#define toneGen3_DAC_RANGE      ((uint8)(1u << 2u) & toneGen3_RANGE_MASK)
#define toneGen3_DAC_POL        ((uint8)(1u >> 1u) & toneGen3_IDIR_MASK)


#endif /* CY_WaveDAC8_toneGen3_H  */

/* [] END OF FILE */
