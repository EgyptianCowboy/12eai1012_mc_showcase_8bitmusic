/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#ifndef UART_SELF
    #define UART_SELF

    typedef struct{
        uint16_t toneFreq;
        uint8_t DT;
        uint8_t ctrlReg;
    }toneData;

    typedef struct{
        toneData tone1;
        toneData tone2;
        toneData tone3;
        uint8_t   vol;
        uint8_t   delay;
    }threeTone;
    
    void initData( threeTone *dataUse )
    {
        dataUse->tone1.toneFreq = 0;
        dataUse->tone1.DT = 0;
        dataUse->tone1.ctrlReg = 0;
        dataUse->tone2.toneFreq = 0;
        dataUse->tone2.DT = 0;
        dataUse->tone2.ctrlReg = 0;
        dataUse->tone3.toneFreq = 0;
        dataUse->tone3.DT = 0;
        dataUse->tone3.ctrlReg = 0;
        dataUse->vol = 0;
        dataUse->delay = 0;
    }
    void setData( int *strData, threeTone *dataUse )
    {
        dataUse->tone1.toneFreq = strData[0];
        dataUse->tone1.DT = strData[1];
        dataUse->tone1.ctrlReg = strData[2];
        dataUse->tone2.toneFreq = strData[3];
        dataUse->tone2.DT = strData[4];
        dataUse->tone2.ctrlReg = strData[5];
        dataUse->tone3.toneFreq = strData[6];
        dataUse->tone3.DT = strData[7];
        dataUse->tone3.ctrlReg = strData[8];
        dataUse->vol = strData[9];
        dataUse->delay = strData[10];
    }
    void collectData( uint8 rx, int *strData, volatile int a, volatile int b );
    
    #endif


/* [] END OF FILE */
