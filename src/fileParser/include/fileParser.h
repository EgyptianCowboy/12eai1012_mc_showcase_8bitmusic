#ifndef FILEPARSER
#define FILEPARSER

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

typedef struct toneData_t {
    uint16_t toneFreq;
    uint8_t DT;
    uint8_t ctrlReg;
} toneData;

typedef struct threeTone_t {
    toneData* tone1;
    toneData* tone2;
    toneData* tone3;
    uint8_t   vol;
    uint8_t   delay;
} threeTone;

toneData* initTone ();
threeTone* initThreeTone();
void destroyTones(threeTone* tones);
uint8_t parseBlock (char* line, FILE* fPtr, threeTone* tones);
uint8_t writeTone (uint8_t ctrl, toneData* tone, char* token);
const char *getFileExt(const char *fileName);
uint8_t checkToneNum(char* token);
uint8_t checkToneCtrl(char* token);
//void readData (threeTone* tones);
void writeData (threeTone* tones, FILE* wPtr);

void printThreeTones(threeTone* tones);

void delNewline(char* str, uint8_t len);
uint8_t checkArg(int argc, char** argv, FILE** wPtr);
char* strlwr(char *str);
char* strupr(char* str);
uint8_t strtou8(char* str, uint8_t len);
uint16_t strtou16(char* str, uint8_t len);

#endif
