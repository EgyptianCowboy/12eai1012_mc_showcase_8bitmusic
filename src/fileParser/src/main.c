#include <stdio.h>
#include <stdlib.h>

#include "../include/fileParser.h"

int main(int argc, char** argv) {
    FILE* rPtr = NULL;
    FILE* wPtr = NULL;
    char* line = NULL;

    if (checkArg(argc, argv, &wPtr)) {
	return EXIT_FAILURE;
    }

    if ((rPtr = fopen(argv[1], "r")) == NULL) {
	fprintf(stderr, "Read file does not exist. Aborting.\n");
	return EXIT_FAILURE;
    }

    threeTone* toneData = initThreeTone();

    while (feof(rPtr)==0) {
	if(parseBlock(line, rPtr, toneData)) {
	    return EXIT_FAILURE;
	}
	writeData(toneData, wPtr);
	//printThreeTones(toneData);
    }
    destroyTones(toneData);
    fclose(rPtr);
    fclose(wPtr);
    return EXIT_SUCCESS;
}
