#include "../include/fileParser.h"

toneData* initTone() {
    toneData* tmpTone = (toneData*) malloc(sizeof(toneData));
    tmpTone->toneFreq = 0;
    tmpTone->DT = 0;
    tmpTone->ctrlReg = 0x10;
    return tmpTone;
}

threeTone* initThreeTone() {
    threeTone* tmpTTone = (threeTone*) malloc(sizeof(threeTone));
    tmpTTone->tone1 = initTone();
    tmpTTone->tone2 = initTone();
    tmpTTone->tone3 = initTone();
    tmpTTone->vol   = 0;
    tmpTTone->delay = 0;
    return tmpTTone;
}


void destroyTones(threeTone *tones) {
    free(tones->tone1);
    free(tones->tone2);
    free(tones->tone3);
    free(tones);
}

uint8_t parseBlock(char *line, FILE *fPtr, threeTone *tones) {
    static uint32_t lineNum = 0;
    size_t len = 0;
    char* token;
    uint8_t startParse = 0;

    while (feof(fPtr)==0) {
	getline(&line, &len, fPtr);
	delNewline(line, strlen(line));
	lineNum++;
	if ((strcmp(strlwr(line), "block end")) == 0) {
	    startParse = 0;
	    return EXIT_SUCCESS;
	} else if ((strcmp(strlwr(line), "block begin")) == 0) {
	    startParse = 1;
	    continue;
	} if (strlen(line) == 0) {
	    continue;
	}

	if (startParse) {
	    token = strtok(line, " ");
	    uint8_t toneIndex = checkToneNum(token);
	    if (toneIndex == 1) {
		fprintf(stderr, "Syntax error at line %d\n", lineNum);
		return EXIT_FAILURE;
	    } else {
		token = strtok(NULL, " ");
		switch (toneIndex & 0xF0) {
		case 0x50:
		    tones->vol = strtou8(token,strlen(token));
		    break;
		case 0x40:
		    tones->delay = strtou8(token,strlen(token));
		    break;
		case 0x10:
		    writeTone(toneIndex,tones->tone1,token);
		    break;
		case 0x20:
		    writeTone(toneIndex,tones->tone2,token);
		    break;
		case 0x30:
		    writeTone(toneIndex,tones->tone3,token);
		    break;
		default:
		    break;
		}
	    }
	} else
	    continue;
    }
    return EXIT_SUCCESS;
}

uint8_t writeTone(uint8_t crtl, toneData* tone, char* token) {
    switch (crtl & 0x0F) {
	// DT
    case 0x02:
	tone->DT = strtou8(token,strlen(token));
	return EXIT_SUCCESS;
	break;
	// Frequency
    case 0x03:
	tone->toneFreq = strtou16(token,strlen(token));
	return EXIT_SUCCESS;
	break;
	// Reset
    case 0x04:
	tone->ctrlReg = (tone->ctrlReg & 0x0F) | ((uint8_t)(*(token)-48) << 4);
	return EXIT_SUCCESS;
	break;
	// Waveform
    case 0x05:
	if (strcmp(strlwr(token), "noise")==0) {
	    tone->ctrlReg = (tone->ctrlReg & 0xF0) | 0x01;
	    return EXIT_SUCCESS;
	} else if (strcmp(strlwr(token), "pulse")==0) {
	    tone->ctrlReg = (tone->ctrlReg & 0xF0) | 0x02;
	    return EXIT_SUCCESS;
	} else if (strcmp(strlwr(token), "saw")==0) {
	    tone->ctrlReg = (tone->ctrlReg & 0xF0) | 0x04;
	    return EXIT_SUCCESS;
	} else if (strcmp(strlwr(token), "tri")==0) {
	    tone->ctrlReg = (tone->ctrlReg & 0xF0) | 0x08;
	    return EXIT_SUCCESS;
	} else
	    return EXIT_FAILURE;
	break;
    }
    return EXIT_FAILURE;
}

const char *getFileExt(const char *fileName) {
    const char* dot = strrchr(fileName, '.');
    return (!dot || dot == fileName) ? "" : dot+1;
}

uint8_t checkToneNum(char *token) {
    uint8_t tmpCheck;
    if(tolower(token[0]) == 'v') {
	assert((((token[1]-48)>0) && ((token[1]-48)<4)) || ((tolower(token[1])-97) == 14));
	if (((tolower(token[1]) - 97) == 14)) {
	    return 0x50;
	} else if (!(tmpCheck = checkToneCtrl(token))) {
	    return EXIT_FAILURE;
	}

	switch (token[1] - 48) {
	case 1:
	    // Voice 1
	    return (0x10 | tmpCheck);
	    break;
	case 2:
	    // Voice 2
	    return (0x20 | tmpCheck);
	    break;
	case 3:
	    // Voice 3
	    return (0x30 | tmpCheck);
	    break;
	default:
	    return EXIT_FAILURE;
	    break;
	}
    } else if (strcmp(strlwr(token), "delay") == 0) {
	return 0x40;
    } else {
	return EXIT_FAILURE;
    }
    return EXIT_FAILURE;
}

uint8_t checkToneCtrl(char *token) {
    switch (tolower(token[2]) - 97) {
	// DT
    case 3:
	return 0x02;
	break;
	// Frequency
    case 5:
	return 0x03;
	break;
	// Reset
    case 17:
	return 0x04;
	break;
	// Waveform
    case 22:
	return 0x05;
	break;
    default:
	return EXIT_FAILURE;
	break;
  }
    return EXIT_FAILURE;
}

void writeData(threeTone* tones, FILE* wPtr) {
  if (wPtr != NULL) {
      fprintf(wPtr,"%d\n",tones->tone1->toneFreq);
      fprintf(wPtr,"%d\n",tones->tone1->DT);
      fprintf(wPtr,"%d\n",tones->tone1->ctrlReg);
      fprintf(wPtr,"%d\n",tones->tone2->toneFreq);
      fprintf(wPtr,"%d\n",tones->tone2->DT);
      fprintf(wPtr,"%d\n",tones->tone2->ctrlReg);
      fprintf(wPtr,"%d\n",tones->tone3->toneFreq);
      fprintf(wPtr,"%d\n",tones->tone3->DT);
      fprintf(wPtr,"%d\n",tones->tone3->ctrlReg);
      fprintf(wPtr,"%d\n",tones->vol);
      fprintf(wPtr,"%d\n",tones->delay);
      //fwrite(tones->tone1, sizeof(toneData), 1, wPtr);
      //fwrite(tones->tone2, sizeof(toneData), 1, wPtr);
      //fwrite(tones->tone3, sizeof(toneData), 1, wPtr);
  }
}

void printThreeTones(threeTone* tones) {
    printf("Tone 1:\n  Frequency %d\n  DT: %d\n  Control reg %x\n\n", tones->tone1->toneFreq, tones->tone1->DT, tones->tone1->ctrlReg);
    printf("Tone 2:\n  Frequency %d\n  DT: %d\n  Control reg %x\n\n", tones->tone2->toneFreq, tones->tone2->DT, tones->tone2->ctrlReg);
    printf("Tone 3:\n  Frequency %d\n  DT: %d\n  Control reg %x\n\n", tones->tone3->toneFreq, tones->tone3->DT, tones->tone3->ctrlReg);
    printf("Volume %d\nDelay %d\n\n\n", tones->vol, tones->delay);
}

void delNewline(char* str, uint8_t len) {
    if(str[len-1] == '\n')
	str[len-1] = '\0';
}

uint8_t checkArg(int argc, char** argv, FILE** wPtr) {
  switch (argc) {
  case 1:
      fprintf(stderr, "Not enough arguments. Aborting.\n");
      return EXIT_FAILURE;
      break;
  case 2:
      if (strcmp("tone", getFileExt(argv[1])) != 0) {
	  fprintf(stderr, "Error. Input file is wrong filetype. Aborting.\n");
	  return EXIT_FAILURE;
      }
      fprintf(stdout, "Only provided input file. Using standard output name.\n");
      if ((*wPtr = fopen("stdWrite", "wb")) == NULL) {
	  fprintf(stderr, "Write file does not exist. Aborting.\n");
	  return EXIT_FAILURE;
      }
      return EXIT_SUCCESS;
      break;
  default:
      if (strcmp("tone", getFileExt(argv[1])) != 0) {
	  fprintf(stderr, "Error. Input file is wrong filetype. Aborting.\n");
	  return EXIT_FAILURE;
      }
      if ((*wPtr = fopen(argv[2], "wb")) == NULL) {
	  fprintf(stderr, "Write file does not exist. Aborting.\n");
	  return EXIT_FAILURE;
      }
      return EXIT_SUCCESS;
      break;
  }
}

char* strlwr(char* str) {
  unsigned char *p = (unsigned char *)str;
  while (*p) {
     *p = tolower((unsigned char)*p);
      p++;
  }
  return str;
}

char* strupr(char* str) {
  unsigned char *p = (unsigned char *)str;
  while (*p) {
     *p = toupper((unsigned char)*p);
      p++;
  }
  return str;
}

uint8_t strtou8(char *str, uint8_t len) {
    uint8_t tmpVal = 0;
    for(uint8_t i=0;i<len;i++){
	tmpVal += (str[i]-48);
	if(i>0)
	    tmpVal *= 10;
    }
    return tmpVal;
}
uint16_t strtou16(char *str, uint8_t len) {
    uint16_t tmpVal = 0;
    for(uint8_t i=0;i<len;i++){
	tmpVal += (str[i]-48);
	if(i>0)
	    tmpVal *= 10;
    }
    return tmpVal;
}
