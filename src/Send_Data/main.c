/**************************************************

file: demo_tx.c
purpose: simple demo that transmits characters to
the serial port and print them on the screen,
exit the program by pressing Ctrl-C

compile with the command: gcc demo_tx.c rs232.c -Wall -Wextra -o2 -o test_tx

**************************************************/

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "rs232.h"

#define STRSIZ 80

int main()
{
    int i=0, t = 0,
        cport_nr=5,        /* /dev/ttyS0 (COM1 on windows) */
        bdrate=9600;       /* 9600 baud */

    char mode[]={'8','N','1',5},
     str[9][20], ch;

    FILE *inp;
    inp = fopen("C:\\Users\\Gebruiker\\Desktop\\Project Micro\\test.txt", "r");

    while(1)
    {
        while( ( ch = fgetc(inp) ) != EOF )
        {
          if(ch  == '\n')
          {
              str[i][t] = ch;
              i++;
              t = 0;
          }
          else
          {
              str[i][t] = ch;
              t++;
          }
        }
        i = 0;
        t = 0;

        if(RS232_OpenComport(cport_nr, bdrate, mode))
        {
          printf("Can not open comport\n");

          return(0);
        }

        for(int q = 0; q < 9; q++)
        {

           do{

                  RS232_SendByte(cport_nr, str[i][t]);

                  printf("sent: %c\n", str[i][t]);

                  #ifdef _WIN32
                      Sleep(1000);
                  #else
                      usleep(1000000);  /* sleep for 1 Second */
                  #endif
                  t++;
            }while(str[i][t]  != '\n');
            i++;
        }
        i = 0;
        t = 0;
    }

    return(0);
}
