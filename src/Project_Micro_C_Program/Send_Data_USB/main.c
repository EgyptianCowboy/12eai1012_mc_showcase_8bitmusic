/**************************************************

file: demo_tx.c
purpose: simple demo that transmits characters to
the serial port and print them on the screen,
exit the program by pressing Ctrl-C

compile with the command: gcc demo_tx.c rs232.c -Wall -Wextra -o2 -o test_tx

**************************************************/

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "rs232.h"

#define STRSIZ 80

int main()
{
  int i=0,
      cport_nr=0,        /* /dev/ttyS0 (COM1 on windows) */
      bdrate=9600;       /* 9600 baud */

  char mode[]={'8','N','1',0},
       str[20][20];

  char in_name[STRSIZ];
  FILE *inp;
  int counter = 0;

  printf("Enter name of file you want to implemant> ");
  scanf("%s", in_name);
  inp = fopen(in_name, "r");
  while(inp == NULL)
  {
      printf("Cannot open %s for input\n", in_name);
      printf("Re-enter file name> ");
      scanf("%s", in_name);
      inp = fopen(in_name, "r");
      if(counter >= 5)
      {
          printf("You entered the wrong filename to many times\n");
          exit(EXIT_FAILURE);
      }
      counter += 1;
  }
  counter = 0;

  int i = 0;
  int t = 0;
  while( ( ch = fgetc(fp) ) != EOF )
  {
    if(check[i] == '>')
    {
        i++;
        t = 0;
    }
    else if(check[i] == '\r')
    {
        i++;
        t = 0;
    }
    else
    {
        strcpy(str[i][t], ch);
        t++;
    }
  }
  i = 0;
  t = 0;

  if(RS232_OpenComport(cport_nr, bdrate, mode))
  {
    printf("Can not open comport\n");

    return(0);
  }

  while(1)
  {
    RS232_cputs(cport_nr, str[i]);

    printf("sent: %s\n", str[i]);

#ifdef _WIN32
    Sleep(1000);
#else
    usleep(1000000);  /* sleep for 1 Second */
#endif

    i++;

    if( i == 20 )
    {
        break;
    }
  }

  return(0);
}
