/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "ToneGen.h"

void `$INSTANCE_NAME`_start() {}
uint8 `$INSTANCE_NAME`_readOutLevel() {}
uint8 `$INSTANCE_NAME`_readCtrl() {
    return `$INSTANCE_NAME`_ctrl;
}
uint8 `$INSTANCE_NAME`_readStepTriSaw() {
    return `$INSTANCE_NAME`_stepTriSaw;
}
uint16 `$INSTANCE_NAME`_readClkCnt() {
    return `$INSTANCE_NAME`_clkCnt;
}
uint16 `$INSTANCE_NAME`_readDtNum() {
    return `$INSTANCE_NAME`_dtNum;
}

void `$INSTANCE_NAME`_writeCtrl(uint8 ctrl) {
    `$INSTANCE_NAME`_ctrl = ctrl;
}
void `$INSTANCE_NAME`_writeStepTriSaw(uint8 stepTriSaw) {}
void `$INSTANCE_NAME`_writeClkCnt(uint16 clkCnt) {}
void `$INSTANCE_NAME`_writeDtNum(uint16 dtNum) {}
/* [] END OF FILE */
