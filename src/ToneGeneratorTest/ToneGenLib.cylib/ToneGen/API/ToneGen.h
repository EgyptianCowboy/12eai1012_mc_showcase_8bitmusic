/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"

#define `$INSTANCE_NAME`_ctrl                       `$ctrl`u
#define `$INSTANCE_NAME`_stepTriSaw                 `$stepTriSaw`u
#define `$INSTANCE_NAME`_clkCnt                     `$clkCnt`u
#define `$INSTANCE_NAME`_dtNum                      `$dtNum`u

void `$INSTANCE_NAME`_start();
uint8 `$INSTANCE_NAME`_readOutLevel();
uint8 `$INSTANCE_NAME`_readCtrl();
uint8 `$INSTANCE_NAME`_readStepTriSaw();
uint16 `$INSTANCE_NAME`_readClkCnt();
uint16 `$INSTANCE_NAME`_readDtNum();

void `$INSTANCE_NAME`_writeCtrl(uint8 ctrl);
void `$INSTANCE_NAME`_writeStepTriSaw(uint8 stepTriSaw);
void `$INSTANCE_NAME`_writeClkCnt(uint16 clkCnt);
void `$INSTANCE_NAME`_writeDtNum(uint16 dtNum);
/* [] END OF FILE */
