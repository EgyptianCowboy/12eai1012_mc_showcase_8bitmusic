/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"

#define ToneGen_ctrl                       4u
#define ToneGen_stepTriSaw                 10u
#define ToneGen_clkCnt                     3000u
#define ToneGen_dtNum                      1u

void ToneGen_start();
uint8 ToneGen_readOutLevel();
uint8 ToneGen_readCtrl();
uint8 ToneGen_readStepTriSaw();
uint16 ToneGen_readClkCnt();
uint16 ToneGen_readDtNum();

void ToneGen_writeCtrl(uint8 ctrl);
void ToneGen_writeStepTriSaw(uint8 stepTriSaw);
void ToneGen_writeClkCnt(uint16 clkCnt);
void ToneGen_writeDtNum(uint16 dtNum);
/* [] END OF FILE */
