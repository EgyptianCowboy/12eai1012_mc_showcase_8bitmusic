/*******************************************************************************
* File Name: WavePin.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_WavePin_H) /* Pins WavePin_H */
#define CY_PINS_WavePin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "WavePin_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 WavePin__PORT == 15 && ((WavePin__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    WavePin_Write(uint8 value);
void    WavePin_SetDriveMode(uint8 mode);
uint8   WavePin_ReadDataReg(void);
uint8   WavePin_Read(void);
void    WavePin_SetInterruptMode(uint16 position, uint16 mode);
uint8   WavePin_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the WavePin_SetDriveMode() function.
     *  @{
     */
        #define WavePin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define WavePin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define WavePin_DM_RES_UP          PIN_DM_RES_UP
        #define WavePin_DM_RES_DWN         PIN_DM_RES_DWN
        #define WavePin_DM_OD_LO           PIN_DM_OD_LO
        #define WavePin_DM_OD_HI           PIN_DM_OD_HI
        #define WavePin_DM_STRONG          PIN_DM_STRONG
        #define WavePin_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define WavePin_MASK               WavePin__MASK
#define WavePin_SHIFT              WavePin__SHIFT
#define WavePin_WIDTH              1u

/* Interrupt constants */
#if defined(WavePin__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in WavePin_SetInterruptMode() function.
     *  @{
     */
        #define WavePin_INTR_NONE      (uint16)(0x0000u)
        #define WavePin_INTR_RISING    (uint16)(0x0001u)
        #define WavePin_INTR_FALLING   (uint16)(0x0002u)
        #define WavePin_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define WavePin_INTR_MASK      (0x01u) 
#endif /* (WavePin__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define WavePin_PS                     (* (reg8 *) WavePin__PS)
/* Data Register */
#define WavePin_DR                     (* (reg8 *) WavePin__DR)
/* Port Number */
#define WavePin_PRT_NUM                (* (reg8 *) WavePin__PRT) 
/* Connect to Analog Globals */                                                  
#define WavePin_AG                     (* (reg8 *) WavePin__AG)                       
/* Analog MUX bux enable */
#define WavePin_AMUX                   (* (reg8 *) WavePin__AMUX) 
/* Bidirectional Enable */                                                        
#define WavePin_BIE                    (* (reg8 *) WavePin__BIE)
/* Bit-mask for Aliased Register Access */
#define WavePin_BIT_MASK               (* (reg8 *) WavePin__BIT_MASK)
/* Bypass Enable */
#define WavePin_BYP                    (* (reg8 *) WavePin__BYP)
/* Port wide control signals */                                                   
#define WavePin_CTL                    (* (reg8 *) WavePin__CTL)
/* Drive Modes */
#define WavePin_DM0                    (* (reg8 *) WavePin__DM0) 
#define WavePin_DM1                    (* (reg8 *) WavePin__DM1)
#define WavePin_DM2                    (* (reg8 *) WavePin__DM2) 
/* Input Buffer Disable Override */
#define WavePin_INP_DIS                (* (reg8 *) WavePin__INP_DIS)
/* LCD Common or Segment Drive */
#define WavePin_LCD_COM_SEG            (* (reg8 *) WavePin__LCD_COM_SEG)
/* Enable Segment LCD */
#define WavePin_LCD_EN                 (* (reg8 *) WavePin__LCD_EN)
/* Slew Rate Control */
#define WavePin_SLW                    (* (reg8 *) WavePin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define WavePin_PRTDSI__CAPS_SEL       (* (reg8 *) WavePin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define WavePin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) WavePin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define WavePin_PRTDSI__OE_SEL0        (* (reg8 *) WavePin__PRTDSI__OE_SEL0) 
#define WavePin_PRTDSI__OE_SEL1        (* (reg8 *) WavePin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define WavePin_PRTDSI__OUT_SEL0       (* (reg8 *) WavePin__PRTDSI__OUT_SEL0) 
#define WavePin_PRTDSI__OUT_SEL1       (* (reg8 *) WavePin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define WavePin_PRTDSI__SYNC_OUT       (* (reg8 *) WavePin__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(WavePin__SIO_CFG)
    #define WavePin_SIO_HYST_EN        (* (reg8 *) WavePin__SIO_HYST_EN)
    #define WavePin_SIO_REG_HIFREQ     (* (reg8 *) WavePin__SIO_REG_HIFREQ)
    #define WavePin_SIO_CFG            (* (reg8 *) WavePin__SIO_CFG)
    #define WavePin_SIO_DIFF           (* (reg8 *) WavePin__SIO_DIFF)
#endif /* (WavePin__SIO_CFG) */

/* Interrupt Registers */
#if defined(WavePin__INTSTAT)
    #define WavePin_INTSTAT            (* (reg8 *) WavePin__INTSTAT)
    #define WavePin_SNAP               (* (reg8 *) WavePin__SNAP)
    
	#define WavePin_0_INTTYPE_REG 		(* (reg8 *) WavePin__0__INTTYPE)
#endif /* (WavePin__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_WavePin_H */


/* [] END OF FILE */
