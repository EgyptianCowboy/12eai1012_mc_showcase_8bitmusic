/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "ToneGen.h"

void ToneGen_start() {}
uint8 ToneGen_readOutLevel() {}
uint8 ToneGen_readCtrl() {
    return ToneGen_ctrl;
}
uint8 ToneGen_readStepTriSaw() {
    return ToneGen_stepTriSaw;
}
uint16 ToneGen_readClkCnt() {
    return ToneGen_clkCnt;
}
uint16 ToneGen_readDtNum() {
    return ToneGen_dtNum;
}

void ToneGen_writeCtrl(uint8 ctrl) {
    ToneGen_ctrl = ctrl;
}
void ToneGen_writeStepTriSaw(uint8 stepTriSaw) {}
void ToneGen_writeClkCnt(uint16 clkCnt) {}
void ToneGen_writeDtNum(uint16 dtNum) {}
/* [] END OF FILE */
