`timescale 100 ns/ 1 ps

module toneGen_tb();
   reg [15:0] clkCnt;
   reg [15:0] dtNum;
   reg [7:0]  stepTriSaw;
   reg [7:0]  ctrl;
   reg	      clk;

   wire [7:0]   outLevel;
   wire [15:0]	outCnt;
   wire [15:0]	outSw;
   wire [7:0]	outSawTri;

   // duration for each bit = 20 * timescale = 20 * 1 ns  = 20ns
   localparam period = 1;

   toneGen DUT (
		.clkCnt(clkCnt),
		.dtNum(dtNum),
		.stepTriSaw(stepTriSaw),
		.ctrl(ctrl),
		.clk(clk),
		.outLevel(outLevel),
		.outCnt(outCnt),
		.outSw(outSw),
		.outSawTri(outSawTri)
		);
   integer    ii = 20'h00000;

   initial
     begin
	$dumpfile("test.vcd");
	$dumpvars(0,outLevel,clk,outCnt,ctrl,outSw,clkCnt,outSawTri);
	stepTriSaw <= 8'h05;
	clkCnt <= 16'h0BB8;
	clk <= 0;
	ctrl <= 8'h00;
	dtNum <= 16'h03E8;
	#period;
	ctrl <= 8'h10;
	#period;
	#period;
	ctrl <= 8'h01;
	for(ii=0;ii<=20'hFFFFF;ii=ii+1)
	  begin
	     #period;
	  end
	$finish;
     end // initial begin

   always
     #period clk=~clk;




endmodule
