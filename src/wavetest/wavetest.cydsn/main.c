/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE->
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company->
 *
 * ========================================
*/
#include "project.h"
#include <stdlib.h>

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts-> */

    /* Place your initialization/startup code here (e->g-> MyInst_Start()) */
    typedef struct toneArr_t {
        uint8 triArray[255];
        //uint8 sawArray[255];
        uint8 sawArray[255];
        uint8 noiseArray[255];
        uint8 pulse1Array[255];
        uint8 pulse2Array[255];
        uint8 pulse3Array[255];
        uint16 arrSize;
    } toneArr;
    
    toneArr localTone; 
    
    localTone.arrSize=255;
    
   
    for(int16 i=0; i<(localTone.arrSize/2)+1; i++) {
        localTone.triArray[i] = 2*i;
    }
    for(int16 i=(localTone.arrSize/2); i>= 0 ; i--) {
        localTone.triArray[127+(127-i)] = 2*i;
    }
    WaveDAC_Start();
    WaveDAC_1_Start();
    WaveDAC_2_Start();
   
    
    WaveDAC_Stop();
    WaveDAC_1_Stop();
    WaveDAC_2_Stop();
    
    
    
    WaveDAC_Wave1Setup(localTone.triArray,localTone.arrSize);
    WaveDAC_Start();
    
    for(;;)
    {
        /* Place your application code here-> */
    }
}

/* [] END OF FILE */
