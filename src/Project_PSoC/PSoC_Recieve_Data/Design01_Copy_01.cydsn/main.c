/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

struct toneData {
    uint16_t toneFreq;
    uint8_t DT;
    uint8_t ctrlReg;
};

struct threeTone {
    struct toneData tone1;
    struct toneData tone2;
    struct toneData tone3;
    uint8_t   vol;
    uint8_t   delay;
};

struct threeTone data;
 
int main() {
    uint8 rxData;
    int str[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, i = 0, t = 0;
 
    UART_Start();
    
    data.tone1.toneFreq = 0;
    data.tone1.DT = 0;
    data.tone1.ctrlReg = 0;
    data.tone2.toneFreq = 0;
    data.tone2.DT = 0;
    data.tone2.ctrlReg = 0;
    data.tone3.toneFreq = 0;
    data.tone3.DT = 0;
    data.tone3.ctrlReg = 0;
    data.vol = 0;
    data.delay = 0;
 
    for(;;) // endless loop
    {
        rxData = UART_GetChar(); // store received characters in temporary variable
 
        if(rxData) {
            
            if(rxData == 'A')
            {
                i++;
                if(i == 11)
                {
                    data.tone1.toneFreq = str[0];
                    data.tone1.DT = str[1];
                    data.tone1.ctrlReg = str[2];
                    data.tone2.toneFreq = str[3];
                    data.tone2.DT = str[4];
                    data.tone2.ctrlReg = str[5];
                    data.tone3.toneFreq = str[6];
                    data.tone3.DT = str[7];
                    data.tone3.ctrlReg = str[8];
                    data.vol = str[9];
                    data.delay = str[10];
                    i = 0;
                }
                t = 0;
            }
            else
            {
                if(t == 0)
                {
                    str[i] = 0;
                    t++;
                }
                str[i] *= 10;
                str[i] += ((int) rxData - 48);
            }
        }
    }
}

/* [] END OF FILE */
