/**************************************************

file: demo_tx.c
purpose: simple demo that transmits characters to
the serial port and print them on the screen,
exit the program by pressing Ctrl-C

compile with the command: gcc demo_tx.c rs232.c -Wall -Wextra -o2 -o test_tx

**************************************************/

#include <stdlib.h>
#include <stdio.h>

/*#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif*/

#include "rs232.h"

#define STRSIZ 80

int main()
{
    int i=0, t = 0,
        cport_nr=4,        /* /dev/ttyS0 (COM1 on windows) */
        bdrate=9600,
        delay = 0;       /* 9600 baud */

    char mode[]={'8','N','1',0},
     str[11][20], ch;

    FILE *inp;
    inp = fopen("C:\\Users\\Gebruiker\\Documents\\pxl\\Oef_Psoc\\Project_PSoC\\test.txt", "r");
    if(inp == NULL)
    {
        printf("Can't access file\n");
        free(inp);
        fclose(inp);
        exit(EXIT_FAILURE);
    }

        while( ( ch = fgetc(inp) ) != EOF )
        {
          if(ch  == '\n')
          {
              str[i][t] = 'A';
              i++;
              t = 0;
          }
          else
          {
              str[i][t] = ch;
              t++;
          }
        }
        i = 0;
        t = 0;


        for(i = 0; i < 20; i++)
        {
            if(str[10][i] == 'A')
            {
                i = 0;
                t = 0;
                break;
            }
            else
            {
                delay *= 10;
                delay += (str[10][i] - 48);
            }
        }


        if(RS232_OpenComport(cport_nr, bdrate, mode))
        {
          printf("Can not open comport\n");

          return(0);
        }

        for(int q = 0; q < 11; q++)
        {

           do{

                  RS232_SendByte(cport_nr, str[i][t]);

                  printf("sent: %c\n", str[i][t]);

                  #ifdef _WIN32
                      Sleep(delay);
                  #else
                      usleep((delay * 1000));  /* sleep for 1 Second */
                  #endif
                  t++;
            }while(str[i][t]  != 'A');

            RS232_SendByte(cport_nr, str[i][t]);
            printf("sent: %c\n", str[i][t]);

        #ifdef _WIN32
            Sleep(delay);
        #else
            usleep((delay * 1000));  /* sleep for 1 Second */
        #endif

            i++;
            t = 0;
        }
        i = 0;
        RS232_CloseComport(cport_nr);

    return(0);
}
