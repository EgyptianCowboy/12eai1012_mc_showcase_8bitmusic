/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
void sys_init();
 
int main() {
    uint32 rxData;
 
    sys_init();
    Pin_Green_Write(1);
    Pin_Red_Write(1);
 
    for(;;) // endless loop
    {
        rxData = UART_UartGetChar(); // store received characters in temporary variable
 
        if(rxData) { // make sure data is non-zero
            UART_UartPutChar(rxData); // echo characters in terminal window
            // Handle received characters
            //CyDelay(10000);
            if(rxData == 'q')
            {
                Pin_Green_Write(~Pin_Green_Read());
            }
            Pin_Red_Write(~Pin_Red_Read());
        }
    }
}
void sys_init (void) {
    UART_Start();
 
    // Add additional initialization code as desired
}

/* [] END OF FILE */
