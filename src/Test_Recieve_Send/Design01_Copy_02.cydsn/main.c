/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
 
int main() {
    char8 rxData;
    int str[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0}, i = 0, t = 0;
 
    UART_Start();
 
    for(;;) // endless loop
    {
        rxData = UART_UartGetChar(); // store received characters in temporary variable
 
        if(rxData) {
            UART_UartPutChar(rxData);
            
            if(rxData == '\n')
            {
                i++;
                if(i == 9)
                {
                    i = 0;
                }
                t = 0;
            }
            else
            {
                if(t == 0)
                {
                    str[i] = 0;
                    t++;
                }
                str[i] *= 10;
                str[i] += ((int) rxData - 48);
            }
        }
    }
}

/* [] END OF FILE */
