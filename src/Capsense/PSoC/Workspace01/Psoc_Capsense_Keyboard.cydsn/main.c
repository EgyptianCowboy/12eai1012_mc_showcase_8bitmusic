/*******************************************************************************
* File Name: main.c
*
* Version: 2.40
*
* Description:
*  The project explains the usage of CapSense CSD component. The 2 buttons and
*  linear sliders are used as sensing elements. LED displays buttons active
*  state and slider position is shown on LCD.
*
********************************************************************************
* Copyright 2012-2014, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include <project.h>

//void CapSense_DisplayState(void);

/* Needed for Bargraph */
extern uint8 const CYCODE LCD_customFonts[];

uint16 curPosFreq, curPosVol, oldPosVol, oldPosFreq;
uint8 posMatrix[2];

typedef struct toneData_t {
    uint16_t toneFreq;
    uint8_t DT;
    uint8_t ctrlReg;
} toneData;

typedef struct threeTone_t {
    toneData tone1;
    toneData tone2;
    toneData tone3;
    uint8_t   vol;
    uint8_t   delay;
} threeTone;





uint8_t ctrlReg;
/*Bits
7
6
5
4
3
2
1
0
Beschrijving
/
/
Envelope
Reset
Triangle
Sawtooth
Pulse
Noise
*/


/*******************************************************************************
* Function Name: main
********************************************************************************
* Summary:
*  Main function performs following functions:
*   1. Enable global interrupts.
*   2. Initialize CapSense CSD and Start the sensor scanning loop.
*   3. Process scanning results and display it on LCD/LED.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
int main()
{
    /* Enable global interrupts */
    CyGlobalIntEnable;
    
    CapSense_CSD_Start();

    /* Initialize baselines */
    CapSense_CSD_InitializeAllBaselines();

    while(1u)
    {
        /* Check whether the scanning of all enabled widgets is completed. */
        if(0u == CapSense_CSD_IsBusy())
        {
            /* Update all baselines */
            CapSense_CSD_UpdateEnabledBaselines();

            /* Start scanning all enabled sensors */
            CapSense_CSD_ScanEnabledWidgets();
        }

        /* Display CapSense state using LED/LCD */
        //CapSense_DisplayState();
        
        //***********************************************
        
        int sawtooth =0;
    int triangle =0;
    int pulse =0;
    int noise =0;
 
    
    /* Display BUTTON0 state */
    if (CapSense_CSD_CheckIsWidgetActive(CapSense_CSD_BUTTON0__BTN))
    {
        sawtooth=1;
        triangle=0;
        pulse=0;
        noise=0;    
    }

    /* Display BUTTON1 state */
    if (CapSense_CSD_CheckIsWidgetActive(CapSense_CSD_BUTTON1__BTN))
    {
        sawtooth=0;
        triangle=1;
        pulse=0;
        noise=0;   
    }
    if (CapSense_CSD_CheckIsWidgetActive(CapSense_CSD_BUTTON2__BTN))
    {
        sawtooth=0;
        triangle=1;
        pulse=1;
        noise=0;   
    }
    if (CapSense_CSD_CheckIsWidgetActive(CapSense_CSD_BUTTON3__BTN))
    {
        sawtooth =0;
        triangle =1;
        pulse =0;
        noise =1;   
    }
    

    /* Find Slider Position */
    curPosVol = CapSense_CSD_GetCentroidPos(CapSense_CSD_LINEARSLIDER0__LS);
    
    /* Reset position */
    if(curPosVol == 0xFFFFu)
    {
        curPosVol = 0u;
    }

    /* Move bargraph */
    if (curPosVol != oldPosVol)
    {
        oldPosVol = curPosVol;
        
        //curPos is de ingelezen waarde
        
    }
    
    
    
    
     /* Find Slider Position */
    curPosFreq = CapSense_CSD_GetCentroidPos(CapSense_CSD_LINEARSLIDER1__LS);

    /* Reset position */
    if(curPosVol == 0xFFFFu)
    {
        curPosVol = 0u;
    }

    /* Move bargraph */
    if (curPosVol != oldPosVol)
    {
        oldPosVol = curPosVol;
        
        //curPos is de ingelezen waarde
        
        
        
    }
    
    
    
    //ingelezen waarde Matrix
    CapSense_CSD_GetMatrixButtonPos(CapSense_CSD_MATRIXBUTTON0__MB, posMatrix);
    
    
    
    int positiey = posMatrix[0]; //row
    int positiex = posMatrix[1];//colom
    
    int positie;
    
    
    if (positiey == 0){
    
    positie = positiex;

    }
    else if (positiey == 1){

    positie = positiex + 9;

    }
    else if (positiey == 2){
    positie = positie + 17;


    }
    else if (positiey == 3){

    positie = positie + 25;

    }
    else if (positiey == 4){

    positie = positie + 33;

    }
    else if (positiey == 5){

     positie = positie + 41;

    }
    else if (positiey == 6){

    positie = positie + 49;

    }
        
        
        
        
    
    
    ctrlReg = ctrlReg | ( 0 << 5);
    ctrlReg = ctrlReg | ( 0 << 4);
    ctrlReg = ctrlReg | ( triangle << 3);
    ctrlReg = ctrlReg | ( sawtooth << 2);
    ctrlReg = ctrlReg | ( pulse << 1);
    ctrlReg = ctrlReg | ( triangle << 3);
    struct toneData_t toneData; 
    toneData.ctrlReg = ctrlReg;
    toneData.DT = 50;
    toneData.toneFreq= curPosFreq* positie;
    
    struct threeTone_t threeTone;
    threeTone.delay = 0;
    threeTone.tone1 = toneData;
    
    
    /*
    uint16_t toneFreq;
    uitvoer.pulse= pulse;
    uitvoer.sawtooth=sawtooth;
    uitvoer.noise= noise;
    uitvoer.triangle = triangle;
    uitvoer.freq = curPosFreq* positie;
      */  
        
        //********************************************
        
        
        
        
        
        
        
        
        
    }
}

/*******************************************************************************
* Function Name: CapSense_DisplayState
********************************************************************************
* Summary:
*  Function performs following functions:
*   Display Buttons' state using LEDs and Slider state using LCD bargraph
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
//void CapSense_DisplayState()
//{
    
    
//}


/* [] END OF FILE */
 