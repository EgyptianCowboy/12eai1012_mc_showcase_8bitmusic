
#include <project.h>
#include <stdio.h>
#include "ThereminCapSense.h"
#define RAW_SIZE 50

static uint32 raw_array[RAW_SIZE];
static uint32 raw_array2[RAW_SIZE];
static uint16 raw_array_index;
#define SINE_TABLE_SIZE 128
int16 const sine_table[SINE_TABLE_SIZE]={
    0,    //0
    201,  //1
    401,  //2
    601,  //3
    799,  //4
    995,  //5
    1189, //6 2.8125 degrees  per step
    1380, //7 magnitude 4095
    1567, //8
    1750, //9
    1930, //10
    2105, //11
    2275, //12
    2439, //13
    2598, //14
    2750, //15
    2896, //16
    3034, //17
    3165, //18
    3289, //19
    3405, //20
    3512, //21
    3611, //22
    3702, //23
    3783, //24
    3856, //25
    3919, //26
    3972, //27
    4016, //28
    4051, //29
    4075, //30
    4090, //31
    4095, //32
    4090, //33
    4075, //34
    4051, //35
    4016, //36
    3972, //37
    3919, //38
    3856, //39
    3783, //40
    3702, //41
    3611, //42
    3512, //43
    3405, //44
    3289, //45
    3165, //46
    3034, //47    
    2896, //48
    2750, //49
    2598, //50
    2439, //51
    2275, //52 
    2105, //53
    1930, //54
    1750, //55
    1567, //56
    1380, //57
    1189, //58
    995,  //59
    799,  //60
    601,  //61
    401,  //62
    201,  //63
    0,    //64
    -201,  //65
    -401,  //66
    -601,  //67
    -799,  //68
    -995,  //69
    -1189, //70 2.8125 degrees  per step
    -1380, //71 magnitude 4095
    -1567, //72
    -1750, //73
    -1930, //74
    -2105, //75
    -2275, //76
    -2439, //77
    -2598, //78
    -2750, //79
    -2896, //80
    -3034, //81
    -3165, //82
    -3289, //83
    -3405, //84
    -3512, //85
    -3611, //86
    -3702, //87
    -3783, //88
    -3856, //89
    -3919, //90
    -3972, //91
    -4016, //92
    -4051, //93
    -4075, //94
    -4090, //95
    -4095, //96
    -4090, //97
    -4075, //98
    -4051, //99
    -4016, //100
    -3972, //101
    -3919, //102
    -3856, //103
    -3783, //104
    -3702, //105
    -3611, //106
    -3512, //107
    -3405, //108
    -3289, //109
    -3165, //110
    -3034, //111    
    -2896, //112
    -2750, //113
    -2598, //114
    -2439, //115
    -2275, //116 
    -2105, //117
    -1930, //118
    -1750, //119
    -1567, //120
    -1380, //121
    -1189, //122
    -995,  //123
    -799,  //124
    -601,  //125
    -401,  //126
    -201   //127 
};
#define F_150HZ      41231686   // wave angle increment to produce 150Hz tone
#define F_1000HZ    274877907   // wave angle increment to produce 1000Hz tone
#define CAP_SLOPE   58411      // slope to produce about 1000Hz tone when hand is very close to antenna
// wave_angle_inc = CAP_SLOPE * sensor_reading + F_150HZ
static uint32 wave_angle_acc; // DDS waveform angle accumulator
static uint32 wave_angle_inc; // DDS waveform angle increment
static uint32 tone_sensor,volume_sensor;
static uint32 slow_volume_sensor;
#define MAX_VOLUME_SENSOR 255
CY_ISR( tone_isr_Handler)
{
    int32 temp_val;
    int32 temp_volume;
    Debug_LED_Write(~Debug_LED_Read());
    wave_angle_acc += wave_angle_inc;
    temp_val = sine_table[wave_angle_acc>>25]; // look up waveform value from table
    if(slow_volume_sensor > MAX_VOLUME_SENSOR )
    { 
        temp_val = 0;
    }else{
        temp_volume = MAX_VOLUME_SENSOR - slow_volume_sensor; // reduce volume as hand gets closer to sensor
        temp_val = temp_val * temp_volume;
        temp_val = temp_val / MAX_VOLUME_SENSOR;
    }
    //Debug.log(temp_val);
    Tone_Gen_ClearInterrupt( Tone_Gen_INTR_MASK_TC );    
}
int main()
{
 //   uint8 interruptState;
    uint32 i;
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    /* Initialize hardware resources */

    //Audio_Out_Start();
    Tone_Gen_Start();
    tone_isr_StartEx( tone_isr_Handler );

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    raw_array_index = 0;
    static uint32 temp_sensor;
    static uint32 temp_sensor2;
    static uint32 first_tone_sensor,first_volume_sensor;
    ThereminCapSense_FLASH_WD_STRUCT const *ptrFlashWdgt;
    ThereminCapSense_FLASH_WD_STRUCT const *ptrFlashWdgt2;
    ThereminCapSense_RAM_SNS_STRUCT *ptrSns;
    ThereminCapSense_RAM_SNS_STRUCT *ptrSns2;
          
    ThereminCapSense_Start();
 
    ptrFlashWdgt = ThereminCapSense_dsFlash.wdgtArray;
    ptrFlashWdgt2 = ptrFlashWdgt+ 1; //(ThereminCapSense_dsFlash.wdgtArray)[1];
    for(;;)
    {
        /* Place your application code here. */
        /* Do this only when a scan is done */
//interruptState = CyEnterCriticalSection();
        if(ThereminCapSense_NOT_BUSY == ThereminCapSense_IsBusy())
        {
            //CyExitCriticalSection(interruptState);
            ThereminCapSense_ProcessAllWidgets(); /* Process all widgets */
 
            ptrSns = ptrFlashWdgt->ptr2SnsRam;
            raw_array[raw_array_index] = (uint32)ptrSns->raw[0];
    
            ptrSns2 = ptrFlashWdgt2->ptr2SnsRam;   
            raw_array2[raw_array_index++] = (uint32)ptrSns2->raw[0];
// Compute sensor averages on every pass    
            temp_sensor = 0;
            temp_sensor2 = 0;
            for(i=0;i<RAW_SIZE;i++){
                temp_sensor += raw_array[i];
                temp_sensor2 += raw_array2[i];
            }
            temp_sensor /= RAW_SIZE ;
            tone_sensor = temp_sensor;
            temp_sensor2 /= RAW_SIZE ;
            volume_sensor = temp_sensor2;
            if (volume_sensor > first_volume_sensor){
              volume_sensor -= first_volume_sensor;
            }else{
              volume_sensor = 0;
            }
            if(volume_sensor>slow_volume_sensor){
                slow_volume_sensor++;
            }else if(volume_sensor < slow_volume_sensor){
                slow_volume_sensor--;
            }
            
// Save the first average as the baseline capacitance sensor values
            if(raw_array_index >= RAW_SIZE){
                raw_array_index = 0;
                if(!first_tone_sensor) first_tone_sensor = tone_sensor;
                if(!first_volume_sensor) first_volume_sensor = volume_sensor;
            }
            if(first_tone_sensor){
                if (tone_sensor > first_tone_sensor){
                    tone_sensor -= first_tone_sensor;
                }else{
                    tone_sensor = 0;
                }
        //calculate waveform increment on every pass         
                wave_angle_inc = CAP_SLOPE * tone_sensor + F_150HZ;
                
            }
    
        ThereminCapSense_ScanAllWidgets(); /* Start next scan */

//      CyExitCriticalSection(interruptState);
        CySysPmSleep(); /* Sleep until scan is finished */
        } // end of if(ThereminCapSense_NOT_BUSY == ThereminCapSense_IsBusy())
    } // end of forever for loop
}

/* [] END OF FILE */
