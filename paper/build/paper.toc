\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {dutch}{}
\contentsline {section}{\numberline {1}Inleiding}{3}{section.1}
\contentsline {section}{\numberline {2}Input}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}CLI Programma}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Fileparser}{4}{subsection.2.2}
\contentsline {section}{\numberline {3}Keyboard}{5}{section.3}
\contentsline {section}{\numberline {4}Communicatie}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}C-programma}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}UART}{6}{subsection.4.2}
\contentsline {section}{\numberline {5}Toongenerator}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Verilog}{7}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Beschrijving van de verilog code}{7}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Tekortkomingen van de verilogblok}{9}{subsubsection.5.1.2}
\contentsline {subsection}{\numberline {5.2}WaveDAC}{9}{subsection.5.2}
\contentsline {section}{\numberline {6}Conclusie}{13}{section.6}
\contentsline {section}{Appendices}{15}{section*.11}
\contentsline {section}{\numberline {A}Verilog code}{15}{B"ylage.1.A}
